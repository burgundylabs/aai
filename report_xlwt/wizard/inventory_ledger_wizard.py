from openerp.osv import fields, osv
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time

class inventory_ledger(osv.osv_memory):
    _name = "inventory.ledger"
    
    def _get_date_from(self, cr, uid, context=None):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.find(cr, uid, time.strftime('%Y-%m-%d'))
        return period_obj.browse(cr, uid, period_ids[0]).date_start
        
    def _get_account(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        accounts = self.pool.get('account.account').search(cr, uid, [('parent_id', '=', False), ('company_id', '=', user.company_id.id)], limit=1)
        return accounts and accounts[0] or False
    
    def _account_ids(self, cr, uid, context=None):
        return self.pool.get('inventory.balance').allowed_account(cr, uid, self._get_account(cr, uid, context), context)[0]
    
    _columns = {
        'date_from': fields.date('Start Date', required=True),
        'date_to': fields.date('End Date', required=True),
        'target_move': fields.selection([('posted', 'All Posted Entries'),
                                         ('all', 'All Entries'),
                                        ], 'Target Moves', required=True),
        'chart_account_id': fields.many2one('account.account', 'Chart of Account',
                                            help='Select Charts of Accounts', required=True,
                                            domain=[('parent_id', '=', False)]),
        'name': fields.char('File Name', 16),
        'account_ids' : fields.many2many(
            'account.account', string='Accounts Filter')
    }
    
    _defaults = {
            'chart_account_id': _get_account,
            'target_move': 'posted',
            'date_from' : _get_date_from,
            'date_to' : fields.date.today(),
            'account_ids' : _account_ids,
    }
    
    def get_init_bal(self, cr, uid, ids, context=None):
        product = context.get('product_id')
        date = context.get('date')
        query = ''' 
            SELECT
                SUM(CASE \
                    WHEN aml.debit > 0 THEN aml.quantity \
                    WHEN aml.credit > 0 THEN -aml.quantity \
                    WHEN sl1.usage = 'internal' THEN -aml.quantity \
                    ELSE aml.quantity END),
                SUM(aml.debit-aml.credit)
            FROM
                account_move_line aml
                LEFT JOIN account_move am ON (aml.move_id=am.id)
                LEFT JOIN stock_move sm ON (am.stock_move_id=sm.id)
                LEFT JOIN stock_location sl1 ON (sm.location_id=sl1.id)
            WHERE
                aml.product_id = %s AND aml.account_id = %s AND aml.date < to_date('%s','YYYY-MM-DD')''' \
            % (product.id, product.categ_id.property_stock_valuation_account_id.id, date)
        if context.get('move') == 'posted':
            query += " AND  am.state = 'posted'"
        cr.execute(query)
        res = cr.fetchone()
        return res and {'value' : (res[1] or 0.0), 'qty' : (res[0] or 0.0)} or {'value' : 0.0, 'qty' : 0.0}
    
    
    def get_initial_bal(self, cr, uid, ids, context=None):
        if context == None :
            context = {}
        this = self.read(cr, uid, ids)[0]
        context['target_move'] = this['target_move']
        context['date_from'] = '0000-00-00'
        context['date_to'] = context['date']
        context['type'] = 'in'
        cr.execute(self.pool.get('inventory.balance').query_get(cr, uid, context))
        out = cr.fetchone()      
        res = out and {'value_in' : (out[3] or 0.0), 'qty_in' : (out[2] or 0.0)} or\
            {'value_in' : 0.0, 'qty_in' : 0.0}
        context['type'] = 'out'
        cr.execute(self.pool.get('inventory.balance').query_get(cr, uid, context))
        out = cr.fetchone()
        out and res.update({'value_out' : (out[3] or 0.0), 'qty_out' : (out[2] or 0.0)}) or\
             res.update({'value_out' : 0.0, 'qty_out' : 0.0})
        res.update({'value' : res['value_in'] - res['value_out'], 
                    'qty' : res['qty_in'] - res['qty_out']})
        return res
        
    def export(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        this = self.browse(cr, uid, ids[0])
        this_company = this.chart_account_id.company_id
        datas = {}
        datas['ids'] = [this['id']]
        datas['company_name'] = this_company.name + ' - ' + this_company.currency_id.name
        datas['target_move'] = this.target_move == 'all' and 'All Entries' or 'All Posted Entries'
        datas['chart_account_id'] = this.chart_account_id.name
        datas['model'] = self._name
        datas['form'] = this.read()[0]
        datas['date_from'] = (datetime.strptime(this.date_from, '%Y-%m-%d') - relativedelta(days=1)).strftime('%Y-%m-%d')
        datas['date_to'] = datetime.strptime(this.date_to, '%Y-%m-%d').strftime('%d %B %Y')
        datas['date_start'] = datetime.strptime(this.date_from, '%Y-%m-%d').strftime('%d %B %Y')
        
        datas['accounts_code'] = ', '.join(map(str, [x.code for x in this['account_ids']]))
        domain = [('date', '<=', this.date_to),
                  ('date', '>=', this.date_from),
                  ('account_id', 'in', this['account_ids'].ids),
                  ('product_id', '!=', False),
                  ]
        
        if this.target_move == 'posted':
            domain.append(('move_id.state', '=', 'posted'))
            
        aml_ids = self.pool.get('account.move.line').search(cr, uid, domain, order='account_id,product_id,date,id')
        
        csv = {}
        for line in self.pool.get('account.move.line').browse(cr, uid, aml_ids):
            if not csv.get(line.account_id.id, False):
                csv[line.account_id.id] = {'name': line.account_id.code + ' - ' + line.account_id.name,
                                           'product' : {}}
                
            if not csv[line.account_id.id]['product'].get(line.product_id.id, False):
                csv[line.account_id.id]['product'][line.product_id.id] = {
                    'name' : line.product_id.code + ' - ' + line.product_id.name,
                    'line' : []
                }
                
            move_sign = False
            if not line.debit and not line.credit:
                move_sign = line.move_id.stock_move_id.location_id.usage == 'internal' and -1 or 1
                
            if csv[line.account_id.id]['product'][line.product_id.id].get('bal', 'False') == 'False':
                context['product_id'] = line.product_id
                context['date'] = datas['date_from']
                context['move'] = this.target_move
                context['account_ids'] = [line.account_id.id]
                csv[line.account_id.id]['product'][line.product_id.id]['bal'] = self.get_initial_bal(cr, uid, ids, context)
                
                
            csv[line.account_id.id]['product'][line.product_id.id]['bal']['qty'] += \
                move_sign and move_sign * line.quantity or (line.debit > 0 and line.quantity) or (line.credit > 0 and -line.quantity) or 0
            csv[line.account_id.id]['product'][line.product_id.id]['bal']['value'] += line.debit - line.credit
                    
            counterpart = ''
            for line2 in line.move_id.line_id:
                counterpart += line.account_id != line2.account_id and line.account_id.code or ''
                
            csv[line.account_id.id]['product'][line.product_id.id]['line'].append([
                datetime.strptime(line.date, '%Y-%m-%d').strftime('%d/%m/%Y'),
                line.period_id.name,
                line.move_id.name,
                line.move_id.journal_id.code,
                line.analytic_account_id.code or '',
                line.partner_id.name or '',
                line.ref or '',
                line.name,
                counterpart,
                line.debit or 0,
                move_sign and move_sign == 1 and line.quantity or (line.debit > 0 and line.quantity) or 0,
                line.credit or 0,
                move_sign and move_sign == -1 and line.quantity or (line.credit > 0 and line.quantity) or 0,
                csv[line.account_id.id]['product'][line.product_id.id]['bal']['value'],
                csv[line.account_id.id]['product'][line.product_id.id]['bal']['qty']
            ])
        
        datas['csv'] = csv
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'inventory.ledger',
            'nodestroy': True,
            'datas': datas,
        }
