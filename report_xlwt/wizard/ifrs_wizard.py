# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time

class ifrs_wizard(osv.osv_memory):
    _name = "ifrs.wizard"
    _description = 'IFRS Report Wizard'

    _columns = {
        'period_id': fields.many2one('account.period', 'Force period',
                                  help=('Fiscal period to assign to the\
                                        invoice. Keep empty to use the period\
                                        of the current date.')),
        'fiscalyear_id' : fields.many2one('account.fiscalyear', 'Fiscal Year',
                                         help='Fiscal Year'),
        'company_id' : fields.many2one('res.company', string='Company',
                                      ondelete='cascade', required=True,
                                      help='Company name'),
        'currency_id' : fields.many2one('res.currency', 'Currency',
                            help=('Currency at which this report will be \
                                  expressed. If not selected will be used the \
                                  one set in the company')),
        'exchange_date': fields.date('Exchange Date', help=('Date of change\
                                                            that will be\
                                                            printed in the\
                                                            report, with\
                                                            respect to the\
                                                            currency of the\
                                                            company')),
        'report_type': fields.selection([
            ('all', 'All Fiscalyear (YtD)'),
            ('per', 'Force Period (MtD)')],
            string='Type', required=True, help=('Indicates if the report it\
                                                will be printed for the entire\
                                                fiscal year, or for a\
                                                particular period')),
        'columns': fields.selection([
                        ('ifrs', 'Two Columns'),
                        ('webkitaccount.ifrs_12', 'Twelve Columns'), ],
                        string='Number of Columns',
                        help='Number of columns that will be printed in the report:'
                        " -Two Colums(02),-Twelve Columns(12)"),
        'target_move': fields.selection([('posted', 'All Posted Entries'),
                                         ('all', 'All Entries'),
                                         ], 'Target Moves', help=('Print All\
                                                                  Accounting\
                                                                  Entries or\
                                                                  just Posted\
                                                                  Accounting\
                                                                  Entries')),
        'name': fields.many2one('ifrs.ifrs', 'Report Name', required=True),
        'expand_mode': fields.selection([
                        ('summary', 'Summary'),
                        ('details', 'Details'), ],
                        string='Mode'),
    }
    
    def _get_fiscalyear(self, cr, uid, context=None):
        if context is None:
            context = {}
        now = time.strftime('%Y-%m-%d')
        company_id = False
        ids = context.get('active_ids', [])
        if ids and context.get('active_model') == 'account.account':
            company_id = self.pool.get('account.account').browse(cr, uid, ids[0], context=context).company_id.id
        else:  # use current company id
            company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        domain = [('company_id', '=', company_id), ('date_start', '<', now), ('date_stop', '>', now)]
        fiscalyears = self.pool.get('account.fiscalyear').search(cr, uid, domain, limit=1)
        return fiscalyears and fiscalyears[0] or False

    _defaults = {
        'report_type': 'all',
        'target_move': 'posted',
        'currency_id': lambda self, cr, uid, c:self.pool.get('res.users').browse(cr, uid, uid).company_id.currency_id.id,
        'company_id': lambda self, cr, uid, c:self.pool.get('res.users').browse(cr, uid, uid).company_id.id,
        'fiscalyear_id': lambda self, cr, uid, c:self.pool.get('account.fiscalyear').find(cr, uid, time.strftime('%Y-%m-%d'), context=c),
        'period_id': lambda self, cr, uid, c:self.pool.get('account.period').find(cr, uid, time.strftime('%Y-%m-%d'), context=c)[0],
        'exchange_date': fields.date.today,
        'columns': 'ifrs',
        'expand_mode': 'summary'
    }
    
    def onchange_report_type(self, cr, uid, ids, report_type, context=None):
        return {'value': {'columns': report_type == 'per' and 'ifrs' or 'webkitaccount.ifrs_12'}}
        
    def export(self, cr, uid, ids, context=None):
        context = context and dict(context) or {}
        
        wizard_ifrs = self.browse(cr, uid, ids[0], context=context)
        this_company = self.pool.get('res.users').browse(cr, uid, uid).company_id
        
        datas = {'active_ids': context.get('active_ids', [])}
        datas['company_name'] = this_company.name + ' - ' + this_company.currency_id.name
        datas['name'] = wizard_ifrs.name.name
        datas['target_move'] = wizard_ifrs.target_move == 'all' and 'All Entries' or 'All Posted Entries'
        datas['exchange_date'] = datetime.strptime(wizard_ifrs.exchange_date,'%Y-%m-%d').strftime('%d/%m/%Y')
        datas['currency_name'] = wizard_ifrs.currency_id.name
        datas['year'] = time.strftime('%y')
        datas['date'] = time.strftime('%d/%m/%Y')
        datas['columns'] = wizard_ifrs.columns == 'ifrs'
        datas['description'] = wizard_ifrs.report_type=='all' and wizard_ifrs.period_id.fiscalyear_id.name or wizard_ifrs.period_id.name
        datas['period_name'] = [x.name for x in wizard_ifrs.fiscalyear_id.period_ids if not x.special]
        
        datas['fiscalyear_id'] = wizard_ifrs.period_id.fiscalyear_id.name
        datas['report_type'] = wizard_ifrs.report_type
        datas['period_id'] = wizard_ifrs.period_id.name
        
        
        
        ifrs_id = wizard_ifrs.name.id
        fy_id = wizard_ifrs.fiscalyear_id.id
        exchange_date = datetime.strptime(wizard_ifrs.exchange_date,'%Y-%m-%d').strftime('%d/%m/%Y')
        currency_wizard = wizard_ifrs.currency_id.id
        target_move = wizard_ifrs.target_move
        period = wizard_ifrs.report_type=='per' and wizard_ifrs.period_id.id or False
        two = wizard_ifrs.columns == 'ifrs' or None
        is_compute = None
        context['expand_mode'] = wizard_ifrs.expand_mode == 'details'
        report_data = self.pool.get('ifrs.ifrs').get_report_data(cr, uid, [ifrs_id], fy_id,
                    exchange_date, currency_wizard,
                    target_move, period, two,
                    is_compute, context)
        
        datas['model'] = 'ifrs.wizard'
        datas['data'] = report_data
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'ifrs.excel',
            'nodestroy': True,
            'datas': datas,
        }
        
ifrs_wizard()
