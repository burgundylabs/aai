from openerp.osv import fields, osv
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time

class inventory_balance(osv.osv_memory):
    _name = "inventory.balance"
    
    def _get_date_from(self, cr, uid, context=None):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.find(cr, uid, time.strftime('%Y-%m-%d'))
        return period_obj.browse(cr, uid, period_ids[0]).date_start
        
    def _get_account(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        accounts = self.pool.get('account.account').search(cr, uid, [('parent_id', '=', False), ('company_id', '=', user.company_id.id)], limit=1)
        return accounts and accounts[0] or False
    
    def _account_ids(self, cr, uid, context=None):
        return self.allowed_account(cr, uid, self._get_account(cr, uid, context), context)[0]
    
    _columns = {
        'date_from': fields.date('Start Date', required=True),
        'date_to': fields.date('End Date', required=True),
        'target_move': fields.selection([('posted', 'All Posted Entries'),
                                         ('all', 'All Entries'),
                                        ], 'Target Moves', required=True),
        'chart_account_id': fields.many2one('account.account', 'Chart of Account',
                                            help='Select Charts of Accounts', required=True,
                                            domain=[('parent_id', '=', False)]),
        'name': fields.char('File Name', 16),
        'account_ids' : fields.many2many(
            'account.account', string='Accounts Filter')
    }
    
    _defaults = {
            'chart_account_id': _get_account,
            'target_move': 'posted',
            'date_from' : _get_date_from,
            'date_to' : fields.date.today(),
            'account_ids' : _account_ids,
    }
    
    def query_get(self, cr, uid, context=None):
    
        filter_state_query = ''
        filter_state_query_after = ''
        if context['target_move'] == 'posted':
            filter_state_query = '''CASE WHEN am.state = 'posted' THEN '''
            filter_state_query_after = ''' ELSE 0 END'''
            
        query = "SELECT pp.id, pp.default_code, SUM(%sCASE WHEN aml.debit > 0 THEN aml.quantity " + \
                "WHEN aml.credit > 0 THEN -aml.quantity %s END%s), SUM(%saml.debit-aml.credit%s) " + \
                "FROM product_product pp LEFT JOIN product_template pt ON (pp.product_tmpl_id = pt.id) " + \
                "LEFT JOIN account_move_line aml ON (aml.product_id=pp.id AND aml.account_id in (" + \
                ','.join(map(str, context['account_ids'])) + ") "
        query_after = ''') LEFT JOIN account_move am ON (aml.move_id=am.id)
                        LEFT JOIN stock_move sm ON (am.stock_move_id=sm.id)
                        LEFT JOIN stock_location sl1 ON (sm.location_id=sl1.id)
                        WHERE pt.type = 'product' %s
                        GROUP BY pp.default_code, aml.account_id, pp.id
                        ORDER BY aml.account_id, pp.default_code'''
        filter_quantity_mutation = "WHEN sl1.usage = 'internal' THEN -aml.quantity ELSE aml.quantity"
        filter_quantity_mutation_in = "WHEN sl1.usage = 'internal' THEN 0 ELSE aml.quantity"
        filter_quantity_mutation_out = "WHEN sl1.usage = 'internal' THEN -aml.quantity ELSE 0"
        
        filter_product = ''
        if context.get('product_id', False):
            filter_product = 'AND pp.id = %s' % context['product_id'].id
        
        if context['type'] == 'init' :
            filter_date_query_initial = """AND date_trunc('day',aml.date) < to_date('%s','YYYY-MM-DD')""" \
                                    % (context['date_from'])
            return query % \
                (filter_state_query, filter_quantity_mutation, filter_state_query_after, filter_state_query, filter_state_query_after)\
                + filter_date_query_initial + query_after % filter_product
        elif context['type'] == 'in' :
            filter_date_query_mutation_in = """ AND date_trunc('day',aml.date) >= to_date('%s','YYYY-MM-DD') 
                                    AND date_trunc('day',aml.date) <= to_date('%s','YYYY-MM-DD') AND aml.credit = 0""" \
                                    % (context['date_from'], context['date_to'])
            return query % \
                (filter_state_query, filter_quantity_mutation_in, filter_state_query_after, filter_state_query, filter_state_query_after)\
                + filter_date_query_mutation_in + query_after % filter_product
        elif context['type'] == 'out' :
            filter_date_query_mutation_out = """ AND date_trunc('day',aml.date) >= to_date('%s','YYYY-MM-DD') 
                                    AND date_trunc('day',aml.date) <= to_date('%s','YYYY-MM-DD') AND aml.debit = 0""" \
                                    % (context['date_from'], context['date_to'])
            return query % \
                (filter_state_query, filter_quantity_mutation_out, filter_state_query_after, filter_state_query, filter_state_query_after)\
                + filter_date_query_mutation_out + query_after % filter_product
    
    def allowed_account(self, cr, uid, ids, context=None):
        cr.execute("SELECT \
                        CAST(BTRIM(ip.value_reference,'account.account,') AS INT) AS account_id,\
                        aa.code as code\
                    FROM \
                        ir_property ip\
                        LEFT JOIN account_account aa ON (aa.id=CAST(BTRIM(ip.value_reference,'account.account,') AS INT))\
                    WHERE \
                        ip.name='property_stock_valuation_account_id'")
        account_ids = cr.fetchall()
        allowed_acc_ids = self.pool.get('account.account')._get_children_and_consol(cr, uid, ids, context)
        acc_ids = [x[0] for x in account_ids if x[0] != None and x[0] in allowed_acc_ids]
        accounts_code = ', '.join([x[1] for x in account_ids if x[1] != None])
        
        return acc_ids, accounts_code
    
    def export(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        this = self.read(cr, uid, ids)[0]
        this_company = self.pool.get('res.users').browse(cr, uid, uid).company_id
        
        datas = {}
        datas['ids'] = [this['id']]
        datas['form'] = this
        datas['company_name'] = this_company.name + ' - ' + this_company.currency_id.name
        datas['target_move'] = this['target_move'] == 'all' and 'All Entries' or 'All Posted Entries'
        datas['chart_account_id'] = this['chart_account_id'][1][7:]
        datas['model'] = self._name
        datas['date_from'] = (datetime.strptime(this['date_from'], '%Y-%m-%d') - relativedelta(days=1)).strftime('%d %B %Y')
        datas['date_to'] = datetime.strptime(this['date_to'], '%Y-%m-%d').strftime('%d %B %Y')
        datas['date_start'] = datetime.strptime(this['date_from'], '%Y-%m-%d').strftime('%d %B %Y')
        datas['accounts_code'] = ', '.join(map(str, [x.code for x in self.pool.get('account.account').browse(cr, uid, this['account_ids'])]))
        
        context['target_move'] = this['target_move']
        context['date_from'] = this['date_from']
        context['date_to'] = this['date_to']
        context['account_ids'] = this['account_ids']
        
        context['type'] = 'init'
        cr.execute(self.query_get(cr, uid, context))
        initial = cr.fetchall()
        context['type'] = 'in'
        cr.execute(self.query_get(cr, uid, context))
        mutation_in = cr.fetchall()
        context['type'] = 'out'
        cr.execute(self.query_get(cr, uid, context))
        mutation_out = cr.fetchall()
        datas['csv'] = {}
        datas['account'] = {}
        
        tables = [initial, mutation_in, mutation_out]
        product_ids = list(set([x[0] for table in tables for x in table]))
        for product_id in self.pool.get('product.product').browse(cr, uid, product_ids):
            account_id = product_id.categ_id.property_stock_valuation_account_id
            if account_id.id not in this['account_ids']:
                continue
            if not datas['account'].get(account_id.id, False):
                datas['account'][account_id.id] = account_id.code + ' - ' + account_id.name
                
            if not datas['csv'].get(product_id.id, False):
                datas['csv'][product_id.id] = {'account_id':account_id.id,
                                               'data':[product_id.default_code, product_id.name, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]}
        
        count = 0
        for table in tables:
            for data in table:
                if datas['csv'].get(data[0], False):
                    sign = count>2 and -1 or 1
                    datas['csv'][data[0]]['data'][count + 2] = data[2] and sign*data[2]
                    datas['csv'][data[0]]['data'][count + 3] = data[3] and sign*data[3]
            count += 2
            
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'inventory.balance',
            'nodestroy': True,
            'datas': datas,
        }
