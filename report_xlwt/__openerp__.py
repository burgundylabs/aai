{
    "name": "Inventory Report",
    "version": "1.0",
    "depends": ['account','report','ifrs_report'],
    "author":"PT. VISI",
    "category":"Report",
    "description" : """xls Report""",
    'data': [
        'wizard/inventory_balance_wizard_view.xml',
        'wizard/inventory_ledger_wizard_view.xml',
        'wizard/ifrs_wizard_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
