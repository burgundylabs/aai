import re
import time
import xlwt
from openerp.report import report_sxw
from report_engine_xls import report_xls
from openerp.tools.translate import _
 
class ReportStatus(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        super(ReportStatus, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'cr': cr,
            'uid': uid,
            'time': time,
        })
 
class ifrs_xls(report_xls):
 
    def generate_xls_report(self, parser, _xs, data, obj, wb):
        ws = wb.add_sheet((data['name']))
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        
        if data['report_type'] == 'all':
            period = 'All Fiscal Years (YtD)'
        else:
            period = data['period_id'] + ' (MtD)'
            
        title_style = xlwt.easyxf(_xs['xls_title'])
        ws.write_merge(0, 0, 0, 1, data['name'] + ' - ' + data['company_name'], title_style)
                 
        cell_style_center = xlwt.easyxf(_xs['bold'] + _xs['fill_blue'] + _xs['borders_all'] + _xs['center'])               
        ws.write_merge(2, 2, 0, 1, 'Fiscal Years', cell_style_center)
        ws.write_merge(2, 2, 2, 4, 'Type', cell_style_center)
        ws.write_merge(2, 2, 5, 7, 'Target Moves', cell_style_center)

        cell_style_param = xlwt.easyxf(_xs['borders_all'] + _xs['wrap'] + _xs['top'])  
        ws.row(3).height_mismatch = True
        ws.row(3).height = 28*20         
        ws.write_merge(3, 3, 0, 1, data['fiscalyear_id'], cell_style_param)
        ws.write_merge(3, 3, 2, 4, "Period : " +period, cell_style_param)
        ws.write_merge(3, 3, 5, 7, data['target_move'], cell_style_param)
        
        c_hdr_cell_style = xlwt.easyxf(_xs['bold'] + _xs['fill_blue'] + _xs['borders_all'])
        ws.col(0).width = 256 * 55
        ws.write(5, 0, 'DESCRIPTION', c_hdr_cell_style)
        if data['columns']:
            ws.write(5, 1, data['description'], c_hdr_cell_style)
            ws.col(1).width = 256 * 18
        else:
            for i in range(0, len(data['period_name'])):
                ws.col(i + 1).width = 256 * 18
                ws.write(5, i + 1, data['period_name'][i], c_hdr_cell_style)
                
        abstract_cell_style = xlwt.easyxf(_xs['bold'] + _xs['fill'] + _xs['borders_all'])
        total_cell_style = xlwt.easyxf(_xs['fill'] + _xs['borders_all'], num_format_str=report_xls.decimal_format)
        detail_cell_style = xlwt.easyxf(_xs['borders_all'], num_format_str=report_xls.decimal_format)
        
        def get_style(row_type):
            if row_type == 'total' :
                return total_cell_style
            return detail_cell_style
        
        row_count = 6
        for dat in data['data']:
            if dat['type'] == 'abstract':
                ws.row(row_count).height_mismatch = True
                ws.row(row_count).height = 20 * 4
                row_count += 1
                ws.write_merge(row_count, row_count, 0, data['columns'] and 1 or 12, dat['name'], abstract_cell_style)
                row_count += 1
                continue
            ws.write(row_count, 0, dat['name'], get_style(dat['type']))
            col_count = 1
            if dat.get('amount', 'false') != 'false':
                ws.write(row_count, col_count, dat['amount'], get_style(dat['type']))
            else:
                for per in range(0, len(dat['period'])):
                    ws.write(row_count, col_count, dat['period'][str(per + 1)], get_style(dat['type']))
                    col_count += 1
            row_count += 1
        pass
 
ifrs_xls('report.ifrs.excel', 'ifrs.ifrs', 'addons/report_xlwt/report/report_excel.mako', parser=ReportStatus, header=False)
