import re
import time
import xlwt
from openerp.report import report_sxw
from report_engine_xls import report_xls
from openerp.tools.translate import _
 
class ReportStatus(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        super(ReportStatus, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'cr': cr,
            'uid': uid,
            'time': time,
        })
        
columns = [
              ['Code', 8],
              ['Product Name', 53],
              ['Qty', 9],
              ['Price/Unit', 13],
              ['Value', 17],
              ['Qty', 9],
              ['Price/Unit', 13],
              ['Value', 17],
              ['Qty', 9],
              ['Price/Unit', 13],
              ['Value', 17],
              ['Qty', 9],
              ['Price/Unit', 13],
              ['Value', 17],
          ]

col_map = [0, 1, 2, 4, 5, 7, 8, 10]
 
class inventory_balance_xls(report_xls):
 
    def generate_xls_report(self, parser, _xs, data, obj, wb):
        ws = wb.add_sheet(('Inventory Balance'))
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        ws.set_panes_frozen(True)
        ws.set_horz_split_pos(7) 
        ws.set_vert_split_pos(2)
 
        title_style = xlwt.easyxf(_xs['xls_title'])
        ws.write_merge(0, 0, 0, 1, 'INVENTORY BALANCE - ' + data['company_name'], title_style)
                
        cell_style_center = xlwt.easyxf(_xs['bold'] + _xs['fill_blue'] + _xs['borders_all'] + _xs['center'])               
        ws.write_merge(2, 2, 0, 1, 'Periods Filter', cell_style_center)
        ws.write_merge(2, 2, 2, 5, 'Accounts Filter', cell_style_center)
        ws.write_merge(2, 2, 6, 7, 'Target Moves', cell_style_center)
        ws.write_merge(2, 2, 8, 10, 'Charts of Account', cell_style_center)
        
        cell_style_param = xlwt.easyxf(_xs['borders_all'] + _xs['wrap'] + _xs['top'])  
        ws.row(3).height_mismatch = True
        ws.row(3).height = 20 * 20         
        ws.write_merge(3, 3, 0, 1, data['date_start'] + ' - ' + data['date_to'], cell_style_param)
        ws.write_merge(3, 3, 2, 5, data['accounts_code'], cell_style_param)
        ws.write_merge(3, 3, 6, 7, data['target_move'], cell_style_param)
        ws.write_merge(3, 3, 8, 10, data['chart_account_id'], cell_style_param)
        
        ll_cell_style = xlwt.easyxf(_xs['borders_all'])
        ll_cell_style_dec = xlwt.easyxf(_xs['borders_all'], num_format_str=report_xls.decimal_format)
        c_hdr_cell_style = xlwt.easyxf(_xs['bold'] + _xs['fill'] + _xs['borders_all'])
        c_hdr_cell_style_dec = xlwt.easyxf(_xs['bold'] + _xs['fill'] + _xs['borders_all'], num_format_str=report_xls.decimal_format)
        c_hdr_cell_style_center = xlwt.easyxf(_xs['bold'] + _xs['fill'] + _xs['borders_all'] + _xs['center'])
        row_count = 5
        for account_id in data['account']:
            ws.write_merge(row_count, row_count, 0, 1, data['account'][account_id], c_hdr_cell_style)
            ws.write_merge(row_count, row_count, 2, 4, data['date_from'], c_hdr_cell_style_center)
            ws.write_merge(row_count, row_count, 5, 7, 'IN', c_hdr_cell_style_center)
            ws.write_merge(row_count, row_count, 8, 10, 'OUT', c_hdr_cell_style_center)
            ws.write_merge(row_count, row_count, 11, 13, data['date_to'], c_hdr_cell_style_center)
            row_count += 1
            col_count = 0
            for column in columns:
                ws.col(col_count).width = 256 * column[1]
                ws.write(row_count, col_count, column[0], c_hdr_cell_style)
                col_count += 1
            row_count += 1
            row_start = row_count
            for product_id in data['csv']:
                if data['csv'][product_id]['account_id'] != int(account_id):
                    continue
                col_count = 0
                for x in data['csv'][product_id]['data']:
                    ws.write(row_count, col_map[col_count], x or 0.0, col_count>1 and ll_cell_style_dec or ll_cell_style)
                    col_count += 1
                param_list = (row_count + 1, row_count + 1, row_count + 1)
                ws.write(row_count, 11, xlwt.Formula('C%s+F%s-I%s' % param_list), ll_cell_style)
                ws.write(row_count, 13, xlwt.Formula('E%s+H%s-K%s' % param_list), ll_cell_style)
                for col_count in [3,6,9,12]:
                    cell1 = xlwt.Utils.rowcol_to_cell(row_count, (col_count - 1))
                    cell2 = xlwt.Utils.rowcol_to_cell(row_count, col_count + 1)
                    ws.write(row_count, col_count, xlwt.Formula('IF(' + cell1 + '=0;0;' + cell2 + '/' + cell1 + ')'), ll_cell_style)
                row_count += 1
                
            ws.write_merge(row_count, row_count, 0, 1, data['account'][account_id], c_hdr_cell_style)
            ws.write_merge(row_count, row_count, 2, 3, 'Beginning', c_hdr_cell_style)
            cell_start = xlwt.Utils.rowcol_to_cell(row_start, 4)
            cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 4)
            ws.write(row_count, 4, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style_dec)
            ws.write_merge(row_count, row_count, 5, 6, 'In', c_hdr_cell_style)
            cell_start = xlwt.Utils.rowcol_to_cell(row_start, 7)
            cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 7)
            ws.write(row_count, 7, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style_dec)
            ws.write_merge(row_count, row_count, 8, 9, 'Out', c_hdr_cell_style)
            cell_start = xlwt.Utils.rowcol_to_cell(row_start, 10)
            cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 10)
            ws.write(row_count, 10, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style_dec)
            ws.write_merge(row_count, row_count, 11, 12, 'Ending', c_hdr_cell_style)
            cell_start = xlwt.Utils.rowcol_to_cell(row_start, 13)
            cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 13)
            ws.write(row_count, 13, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style_dec)
            row_count += 2
            
#         ws.write_merge(5, 5, 2, 4, data['date_from'], c_hdr_cell_style_center)
#         ws.write_merge(5, 5, 5, 7, 'IN', c_hdr_cell_style_center)
#         ws.write_merge(5, 5, 8, 10, 'OUT', c_hdr_cell_style_center)
#         ws.write_merge(5, 5, 11, 13, data['date_to'], c_hdr_cell_style_center)
#         
#         cols_specs = [
#                       ('Code', 1, 39, 'text', lambda x, d, p: x[0]),
#                       ('Product Name', 1, 254, 'text', lambda x, d, p: x[1]),
#                       ('Qty', 1, 45, 'number', lambda x, d, p: x[2]),
#                       ('Price/Unit', 1, 62, 'number', lambda x, d, p: x[3]),
#                       ('Value', 1, 80, 'number', lambda x, d, p: x[4]),
#                       ('Qty', 1, 45, 'number', lambda x, d, p: x[5]),
#                       ('Price/Unit', 1, 62, 'number', lambda x, d, p: x[6]),
#                       ('Value', 1, 65, 'number', lambda x, d, p: x[7]),
#                       ('Qty', 1, 45, 'number', lambda x, d, p: x[8]),
#                       ('Price/Unit', 1, 62, 'number', lambda x, d, p: x[9]),
#                       ('Value', 1, 65, 'number', lambda x, d, p: x[10]),
#                       ('Qty', 1, 45, 'number', lambda x, d, p: x[11]),
#                       ('Price/Unit', 1, 62, 'number', lambda x, d, p: x[12]),
#                       ('Value', 1, 80, 'number', lambda x, d, p: x[13]),
#         ]
#         title = self.xls_row_template(cols_specs, [x[0] for x in cols_specs])
#         
#         self.xls_write_row_header(ws, 6, title, c_hdr_cell_style, set_column_size=True)
#         
#         ll_cell_style = xlwt.easyxf(_xs['borders_all'],
#             num_format_str=report_xls.decimal_format)
#         row_count = 7
#         for x in data['csv']:
#             for col_count in range(0, len(x)):
#                 if col_count % 3 != 0 and col_count not in [11, 13] or col_count == 0:
#                     if not x[col_count] and col_count > 1 :
#                         x[col_count] = 0
#                     ws.write(row_count, col_count, x[col_count], ll_cell_style)
#                 if col_count % 3 == 0 and col_count != 0:
#                     cell1 = xlwt.Utils.rowcol_to_cell(row_count, (col_count - 1))
#                     cell2 = xlwt.Utils.rowcol_to_cell(row_count, col_count + 1)
#                     ws.write(row_count, col_count, xlwt.Formula('IF(' + cell1 + '=0;0;' + cell2 + '/' + cell1 + ')'), ll_cell_style)
#             param_list = (row_count + 1, row_count + 1, row_count + 1)
#             ws.write(row_count, 11, xlwt.Formula('C%s+F%s-I%s' % param_list), ll_cell_style)
#             ws.write(row_count, 12, xlwt.Formula('IF(L%s=0;0;N%s/L%s)' % param_list), ll_cell_style)
#             ws.write(row_count, 13, xlwt.Formula('E%s+H%s-K%s' % param_list), ll_cell_style)
#             row_count += 1
#         
#         ws.write_merge(row_count, row_count, 2, 3, 'Beginning', c_hdr_cell_style)
#         cell_start = xlwt.Utils.rowcol_to_cell(7, 4)
#         cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 4)
#         ws.write(row_count, 4, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style)
#         ws.write_merge(row_count, row_count, 5, 6, 'In', c_hdr_cell_style)
#         cell_start = xlwt.Utils.rowcol_to_cell(7, 7)
#         cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 7)
#         ws.write(row_count, 7, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style)
#         ws.write_merge(row_count, row_count, 8, 9, 'Out', c_hdr_cell_style)
#         cell_start = xlwt.Utils.rowcol_to_cell(7, 10)
#         cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 10)
#         ws.write(row_count, 10, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style)
#         ws.write_merge(row_count, row_count, 11, 12, 'Ending', c_hdr_cell_style)
#         cell_start = xlwt.Utils.rowcol_to_cell(7, 13)
#         cell_end = xlwt.Utils.rowcol_to_cell(row_count - 1, 13)
#         ws.write(row_count, 13, xlwt.Formula('sum(' + cell_start + ':' + cell_end + ')'), c_hdr_cell_style)
        pass
 
inventory_balance_xls('report.inventory.balance', 'account.move.line', 'addons/report_xlwt/report/report_excel.mako', parser=ReportStatus, header=False)
