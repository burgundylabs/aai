import re
import time
import xlwt
from openerp.report import report_sxw
from report_engine_xls import report_xls
from openerp.tools.translate import _
 
class ReportStatus(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        super(ReportStatus, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'cr': cr,
            'uid': uid,
            'time': time,
        })
        
columns = [
              ['Date', 12],
              ['Period', 10],
              ['Entry', 14],
              ['Journal', 12],
              ['Analytic', 12],
              ['Partner', 30],
              ['Reference', 20],
              ['Label', 45],
              ['Counterpart', 20],
              ['Debit', 15],
              ['Qty In', 10],
              ['Credit', 15],
              ['Qty Out', 10],
              ['Balance', 15],
              ['Balance Qty', 12],
          ]
 
class inventory_ledger_xls(report_xls):
 
    def generate_xls_report(self, parser, _xs, data, obj, wb):
        ws = wb.add_sheet(('Inventory Ledger'))
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
 
        title_style = xlwt.easyxf(_xs['xls_title'])
        ws.write_merge(0, 0, 0, 14, 'INVENTORY LEDGER - ' + data['company_name'], title_style)
                
        cell_style_center = xlwt.easyxf(_xs['bold'] + _xs['fill_blue'] + _xs['borders_all'] + _xs['center'])               
        ws.write_merge(2, 2, 0, 1, 'Periods Filter', cell_style_center)
        ws.write_merge(2, 2, 2, 5, 'Accounts Filter', cell_style_center)
        ws.write_merge(2, 2, 6, 7, 'Target Moves', cell_style_center)
        ws.write_merge(2, 2, 8, 10, 'Charts of Account', cell_style_center)
        
        cell_style_param = xlwt.easyxf(_xs['borders_all'] + _xs['wrap'] + _xs['top'])  
        ws.row(3).height_mismatch = True
        ws.row(3).height = 20 * 28
        ws.write_merge(3, 3, 0, 1, data['date_start'] + ' - ' + data['date_to'], cell_style_param)
        ws.write_merge(3, 3, 2, 5, data['accounts_code'], cell_style_param)
        ws.write_merge(3, 3, 6, 7, data['target_move'], cell_style_param)
        ws.write_merge(3, 3, 8, 10, data['chart_account_id'], cell_style_param)
        
        c_hdr_cell_style = xlwt.easyxf(_xs['bold'] + _xs['fill'] + _xs['borders_all'],
            num_format_str=report_xls.decimal_format)
        c_title_cell_style = xlwt.easyxf(_xs['bold'])
        c_cell_style = xlwt.easyxf(_xs['borders_all'],
            num_format_str=report_xls.decimal_format)
        c_italic_cell_style = xlwt.easyxf(_xs['italic'] + _xs['borders_all'])
        
        
        row_count = 5
        for account_id in data['csv']:
            ws.write_merge(row_count, row_count, 0, 14, data['csv'][account_id]['name'], c_title_cell_style)
            row_count += 1
            for product_id in data['csv'][account_id]['product']:
                ws.write_merge(row_count, row_count, 0, 14, data['csv'][account_id]['product'][product_id]['name'], c_title_cell_style)
                row_count += 1
                col_count = 0
                for column in columns:
                    ws.col(col_count).width = 256 * column[1]
                    ws.write(row_count, col_count, column[0], c_hdr_cell_style)
                    col_count += 1
                row_count += 1
                row_start = row_count
                
                if data['csv'][account_id]['product'][product_id]['bal']['qty_in'] or \
                    data['csv'][account_id]['product'][product_id]['bal']['qty_out']:
                    ws.write(row_count, 7, 'Initial Balance', c_italic_cell_style)
                    ws.write(row_count, 9, data['csv'][account_id]['product'][product_id]['bal']['value_in'], c_cell_style)
                    ws.write(row_count, 10, data['csv'][account_id]['product'][product_id]['bal']['qty_in'])
                    ws.write(row_count, 11, data['csv'][account_id]['product'][product_id]['bal']['value_out'], c_cell_style)
                    ws.write(row_count, 12, data['csv'][account_id]['product'][product_id]['bal']['qty_out'], c_cell_style)
                    cell_start = xlwt.Utils.rowcol_to_cell(row_count,9)
                    cell_end = xlwt.Utils.rowcol_to_cell(row_count,11)
                    ws.write(row_count, 13, xlwt.Formula(cell_start+'-'+cell_end), c_cell_style)
                    cell_start = xlwt.Utils.rowcol_to_cell(row_count,10)
                    cell_end = xlwt.Utils.rowcol_to_cell(row_count,12)
                    ws.write(row_count, 14, xlwt.Formula(cell_start+'-'+cell_end), c_cell_style)
                    row_count += 1
                    
                for line in data['csv'][account_id]['product'][product_id]['line']:
                    col_count = 0
                    for line_data in line:
                        ws.write(row_count, col_count, line_data, c_cell_style)
                        col_count += 1
                    row_count += 1
                ws.write_merge(row_count, row_count, 0, 7, data['csv'][account_id]['product'][product_id]['name'], c_hdr_cell_style)
                ws.write(row_count, 8, 'Ending Balance', c_hdr_cell_style)
                cell_start = xlwt.Utils.rowcol_to_cell(row_start,9)
                cell_end = xlwt.Utils.rowcol_to_cell(row_count-1,9)
                ws.write(row_count, 9, xlwt.Formula('sum('+cell_start+':'+cell_end+')'), c_hdr_cell_style)
                cell_start = xlwt.Utils.rowcol_to_cell(row_start,10)
                cell_end = xlwt.Utils.rowcol_to_cell(row_count-1,10)
                ws.write(row_count, 10, xlwt.Formula('sum('+cell_start+':'+cell_end+')'), c_hdr_cell_style)
                cell_start = xlwt.Utils.rowcol_to_cell(row_start,11)
                cell_end = xlwt.Utils.rowcol_to_cell(row_count-1,11)
                ws.write(row_count, 11, xlwt.Formula('sum('+cell_start+':'+cell_end+')'), c_hdr_cell_style)
                cell_start = xlwt.Utils.rowcol_to_cell(row_start,12)
                cell_end = xlwt.Utils.rowcol_to_cell(row_count-1,12)
                ws.write(row_count, 12, xlwt.Formula('sum('+cell_start+':'+cell_end+')'), c_hdr_cell_style)
                cell_start = xlwt.Utils.rowcol_to_cell(row_count,9)
                cell_end = xlwt.Utils.rowcol_to_cell(row_count,11)
                ws.write(row_count, 13, xlwt.Formula(cell_start+'-'+cell_end), c_hdr_cell_style)
                cell_start = xlwt.Utils.rowcol_to_cell(row_count,10)
                cell_end = xlwt.Utils.rowcol_to_cell(row_count,12)
                ws.write(row_count, 14, xlwt.Formula(cell_start+'-'+cell_end), c_hdr_cell_style)
                row_count += 2
        pass
 
inventory_ledger_xls('report.inventory.ledger', 'account.move.line', 'addons/report_xlwt/report/report_excel.mako', parser=ReportStatus, header=False)
