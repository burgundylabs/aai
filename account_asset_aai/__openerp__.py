{
    'name' : 'Asset Custom for AAI/APL',
    'version' : '1.1',
    'author' : 'PT. VISI',
    'category' : 'Accounting',
    'description' : """Additional Field in Asset Management for AAI/APL""",
    'website': 'https://www.visi.co.id',
    'depends' : ['account_asset'],
    'data': [
         "account_asset_view.xml",
         "views/report_asset.xml",
         "asset_report.xml",
    ],
   
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
