from openerp.osv import fields, osv

class budget_invoice(osv.osv_memory):
    
    def log_budget(self, cr, uid, id,context=None):
        #import ipdb;ipdb.set_trace()
        wiz_doc=self.browse(cr,uid,id,context=None)
        log_obj=self.pool.get('brg.budget.log')
        def_cur_id=self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.currency_id
        order_cur_id=wiz_doc.currency_id
        rate=1
        if def_cur_id!=order_cur_id:
            cur_obj=self.pool.get('res.currency')
            rate=cur_obj._get_conversion_rate(cr,uid,order_cur_id,def_cur_id,context=None)
            
        for line in wiz_doc.line_ids:
            to_write={ #UNTUK NGE CREATE
                      'account_id':line.account_id.id,
                      'analytic_id': line.analytic_id.id,
                      'amount': line.amount * rate,
                      'rate': rate,
                      'desc': line.desc,
                      'ir_id': line.inv_id.id,
                      'date': line.inv_id.date_invoice,
                      'currency_id':wiz_doc.currency_id.id,
                      }
            log_obj.create(cr,uid,to_write,context=None)
        return True;
    
    def default_get(self, cr, uid, fields, context=None):
        #import ipdb;ipdb.set_trace();
        res = super(budget_invoice, self).default_get(cr, uid, fields, context=context)
        
        if context is None: context = {}
        inv_ids = context.get('active_ids', [])
        active_model = context.get('active_model')

        if not inv_ids or len(inv_ids) != 1:
            # Partial Picking Processing may only be done for one picking at a time
            return res
        
        inv_doc = self.pool.get('account.invoice').browse(cr, uid, inv_ids, context=context)
        if inv_doc.state in ['draft',
                             'proforma',
                             'proforma2',
                             'cancel',
                             ]:
            raise osv.except_osv(('Warning !'), ('Invoice must be in Open/Paid State'))
        if inv_doc.type not in ['in_invoice',
                             ]:
            raise osv.except_osv(('Warning !'), ('Invoice Type must be Supplier Invoice'))

        items = []

        for line in inv_doc.invoice_line:
            line_item = {
                'account_id': line.account_id.id or False,
                'analytic_id': line.account_analytic_id.id or False,
                'amount': line.price_subtotal or False,
                'desc': line.origin or False,
                'inv_id': line.invoice_id.id or False,
            }
            items.append(line_item)
        res.update(line_ids=items)
        
        res.update({'currency_id':inv_doc.currency_id.id or False})
        
        return res
    
    _name = "budget.invoice"
    _columns = {
        'name':fields.text('Name',), #untuk kemudahan mencari dari budget
        'currency_id': fields.many2one('res.currency', 'Currency',),
        'line_ids': fields.one2many('budget.invoice.line', 'wizard_id','Invoice Lines'), 
    }

class budget_invoice_line(osv.osv_memory):

    _name = "budget.invoice.line"
    _columns = {
        'wizard_id': fields.many2one('budget.invoice', 'Related',), 
        'account_id': fields.many2one('account.account', 'Account',), 
        'inv_id': fields.many2one('account.invoice', 'Invoice',), 
        'analytic_id': fields.many2one('account.analytic.account', 'Cost Center',),
        'rate':fields.float('Rate'),
        'amount':fields.float('Amount',),
        'desc':fields.text('Description',), #untuk kemudahan mencari dari budget
        'name':fields.text('Name',), #untuk kemudahan mencari dari budget
        #kolom relasi ke PO LINE
        #kolom relasi ke Invoice LIne
    }
    _defaults = {
    }
    