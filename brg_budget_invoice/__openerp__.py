{
    'name':'Budget Log from Invoices',
    'author' : 'PT. VISI',
    'installable':True,
    'depends':[
            'account',
            ],

    'data':[
            'wizard/budget_invoice_view.xml',
            'wizard/budget_invoice_button.xml',
            ],

}