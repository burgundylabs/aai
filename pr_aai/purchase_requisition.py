from openerp.osv import osv, fields

class purchase_requisition(osv.Model):
    _inherit = "purchase.requisition"
    _columns = {
        "is_pr": fields.boolean("PR", copy=True, default=lambda self: self._context.get('is_pr', False),),            
    }
    
    _defaults = {
        'name': '/'
    }
        
    def tender_in_progress(self, cr, uid, ids, context=None):
        ctx = context or {}
        for pr in self.browse(cr,uid,ids):
            # Set Name
            if pr.is_pr:
                if pr.name == '/':
                    pr.name = self.pool.get('ir.sequence').get(cr, uid, 'purchase.requisition') or '/'
            else:
                pr.name = self.pool.get('ir.sequence').get(cr, uid, 'purchase.order.requisition') or '/'
        return super(purchase_requisition, self).tender_in_progress(cr, uid, ids, context=ctx)