from openerp.osv import osv, fields
from openerp.tools import float_is_zero
import time
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, workflow

class account_invoice(osv.Model):
    _inherit = 'account.invoice'

    def invoice_validate(self, cr, uid, ids, context=None):
        res = super(account_invoice, self).invoice_validate(cr, uid, ids, context=context)
        purchase_order_obj = self.pool.get('purchase.order')
        # read access on purchase.order object is not required
        if not purchase_order_obj.check_access_rights(cr, uid, 'read', raise_exception=False):
            user_id = SUPERUSER_ID
        else:
            user_id = uid
        po_ids = purchase_order_obj.search(cr, user_id, [('invoice_ids', 'in', ids)], context=context)
        for order in purchase_order_obj.browse(cr, uid, po_ids, context=context):
            purchase_order_obj.message_post(cr, user_id, order.id, body=_("Invoice received"), context=context)
            invoiced = []
            shipped = True
            # for invoice method manual or order, don't care about shipping state
            # for invoices based on incoming shippment, beware of partial deliveries
            if (order.invoice_method == 'picking' and
                    not all(picking.invoice_state in ['invoiced'] for picking in order.picking_ids)):
                shipped = False
            for po_line in order.order_line:
                # Custom
                if [line.invoice_id.state not in ['draft', 'cancel'] for line in po_line.invoice_lines] and all(line.invoice_id.state not in ['draft', 'cancel'] for line in po_line.invoice_lines):
                # End
                    invoiced.append(po_line.id)
            if invoiced and shipped:
                self.pool['purchase.order.line'].write(cr, uid, invoiced, {'invoiced': True})
            workflow.trg_write(uid, 'purchase.order', order.id, cr)
        return res        