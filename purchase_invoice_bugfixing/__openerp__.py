# Modul ini tidak bisa digunakan karena harus meng-inherit dari fungsi yg menggunakan super.
# Harus mengubah source code pada fungsi invoice_validate() seperti yg ada pada modul ini

{
    "name": "Purchase Invoice Bug Fixing",
    "version": "1.0",
    "depends": ['purchase',],
    "author": "PT. VISI",
    "category": "Purchasing",
    "description" : """Bug fixing on validation of purchase order and cancelling purchase order.""",
    'data': [
        "purchase_workflow.xml",
],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : True,
}
