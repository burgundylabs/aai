{
    'name' : 'Purchase Order AAI',
    'version' : '1.1',
    'author' : 'PT. VISI',
    'category' : 'Purchase',
    'description' : """Purchase Order for AAI""",
    'website': 'https://www.visi.co.id',
    'depends' : ['purchase','purchase_requisition'],
    'data': ["purchase_order_view.xml",
             "purchase_sequence.xml",
             "views/report_purchaseorder.xml",
             "security/ir.model.access.csv",
    ],
   
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
