{
    'name' : 'Finance Security',
    'version' : '1.1',
    'author' : 'PT. VISI',
    'category' : 'Accounting & Finance',
    'description' : """Finance Security""",
    'website': 'https://www.visi.co.id',
    'depends' : ['account',
                 'purchase'],
    'data': ['security/sec_view.xml',
             "security/ir.model.access.csv",
             'account_view.xml',
    ],
   
    'installable': True,
    'auto_install': False,
}