{
    "name": "Remove Source Document in Calls for Bid",
    "version": "1.0",
    "depends": ['base','purchase_requisition'],
    "author": "PT. VISI",
    "category": "Setting",
    "description" : """Remove The First Source Doc in Calls for Bid Form View""",
    'data': [
        'cfb_view.xml',
],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
