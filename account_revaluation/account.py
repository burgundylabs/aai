from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from lxml import etree
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
        
class res_company(osv.osv):
    _inherit = 'res.company'
    
    _columns = {
        'unreal_income_currency_exchange_account_id': fields.many2one(
            'account.account',
            string="Unrealized Gain Exchange Rate Account",
            domain="[('type', '=', 'other')]"),
        'unreal_expense_currency_exchange_account_id': fields.many2one(
            'account.account',
            string="Unrealized Loss Exchange Rate Account",
            domain="[('type', '=', 'other')]"),
        'revaluation_journal':fields.many2one('account.journal', 'Monthly Revaluation Journal'),
        'realized_analytic_id':fields.many2one('account.analytic.account', 'Realized G/L Analytic Account')
    }
    
class account_config_settings(osv.osv_memory):
    _inherit = 'account.config.settings'
     
    _columns = {
        'unreal_income_currency_exchange_account_id': fields.related(
            'company_id', 'unreal_income_currency_exchange_account_id',
            type='many2one',
            relation='account.account',
            string="Unrealized Gain Exchange Rate Account",
            domain="[('type', '=', 'other')]"),
        'unreal_expense_currency_exchange_account_id': fields.related(
            'company_id', 'unreal_expense_currency_exchange_account_id',
            type="many2one",
            relation='account.account',
            string="Unrealized Loss Exchange Rate Account",
            domain="[('type', '=', 'other')]"),
        'income_currency_exchange_account_id': fields.related(
            'company_id', 'income_currency_exchange_account_id',
            type='many2one',
            relation='account.account',
            string="Realized Gain Exchange Rate Account",
            domain="[('type', '=', 'other'), ('company_id', '=', company_id)]]"),
        'expense_currency_exchange_account_id': fields.related(
            'company_id', 'expense_currency_exchange_account_id',
            type="many2one",
            relation='account.account',
            string="Realized Loss Exchange Rate Account",
            domain="[('type', '=', 'other'), ('company_id', '=', company_id)]]"),
        'revaluation_journal': fields.related(
            'company_id', 'revaluation_journal',
            type="many2one",
            relation='account.journal',
            string="Monthly Revaluation Journal"),
        'realized_analytic_id': fields.related(
            'company_id', 'realized_analytic_id',
            type="many2one",
            relation='account.analytic.account',
            string="Realized G/L Analytic Account",
            domain="[('type', '=', 'normal')]"),
    }
    
    def onchange_company_id(self, cr, uid, ids, company_id, context=None):
        res = super(account_config_settings, self).onchange_company_id(cr, uid, ids, company_id, context=context)
        if company_id:
            company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
            res['value'].update({
                'unreal_income_currency_exchange_account_id': (company.unreal_income_currency_exchange_account_id
                    and company.unreal_income_currency_exchange_account_id.id or False),
                'unreal_expense_currency_exchange_account_id': (company.unreal_expense_currency_exchange_account_id
                    and company.unreal_expense_currency_exchange_account_id.id or False),
                'revaluation_journal': (company.revaluation_journal
                    and company.revaluation_journal.id or False),
                'realized_analytic_id': (company.realized_analytic_id
                    and company.realized_analytic_id.id or False),
                })
        else: 
            res['value'].update({
                'unreal_income_currency_exchange_account_id': False,
                'unreal_expense_currency_exchange_account_id': False,
                'revaluation_journal': False,
                'realized_analytic_id': False,
                })
        return res
    
class revaluation_currency(osv.osv_memory):
    _name = 'revaluation.currency'
    
    def _get_period(self, cr, uid, context=None):
        """Return default period value"""
        period_ids = self.pool.get('account.period').find(cr, uid, context=context)
        return period_ids and period_ids[0] or False
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period to Revalue'),
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic Account'),
    }
     
    _defaults = {
        'period_id': _get_period,
    }

    def revaluate(self, cr, uid, ids, context=None):
        acc_mv_obj = self.pool.get('account.move')
        acc_mv_line_obj = self.pool.get('account.move.line')
        acc_acc_obj = self.pool.get('account.account')
        acc_period_obj = self.pool.get('account.period')
        currency_obj = self.pool.get('res.currency')
        
        period = context.get('period_id', False)
        if not period:
            period = self.read(cr, uid, ids)[0]['period_id'][0]
        period_id = acc_period_obj.browse(cr, uid, period, context)
        company_id = period_id.company_id
        company_currency = company_id.currency_id
                
        journal_id = company_id.revaluation_journal
        if not journal_id :
            raise osv.except_osv(_('Warning !'), _('Please define Revaluation Journal in Settings Menu!'))
        
        payable_accounts = acc_acc_obj.search(cr, uid, [('type', '=', 'payable')])
        liquid_accounts = acc_acc_obj.search(cr, uid, [('type', '=', 'liquidity'), ('currency_id', '!=', False), ('currency_id', '!=', company_currency.id)])
        if len(payable_accounts + liquid_accounts) == 0:
            raise osv.except_osv(_('Warning !'), _('There is no account to revalue!'))
        
        cr.execute('''select
                        account_id,
                        currency_id,
                        sum(amount_currency),
                        sum(debit - credit)                    
                    from
                        account_move_line
                        join account_period ap on (period_id = ap.id and ap.special = false)
                    where
                        currency_id IS NOT NULL and account_id IN %s and date <= %s
                    group by 
                        account_id,currency_id''', (tuple(payable_accounts + liquid_accounts), period_id.date_stop,))
        out = cr.fetchall()
        
        if len(out) == 0:
            return False
        
        move_id = False
        total = 0
        
        for data in out:
            context['date'] = period_id.date_stop
            new_amt_total = currency_obj.compute(cr, uid, data[1], company_currency.id, data[2], context=context)
            diff = new_amt_total - data[3]
            total += diff
            if diff != 0:
                if not move_id:
                    move_id = acc_mv_obj.create(cr, uid, {
                                                            'journal_id': journal_id.id,
                                                            'ref': period_id.name + " Revaluation",
                                                            'date': period_id.date_stop,
                                                            'period_id': period,
                                                            'reversal_state' : '2breversed',
                                                            'reversal_date' : (datetime.strptime(period_id.date_stop, '%Y-%m-%d') + relativedelta(days=1)).strftime('%Y-%m-%d')
                                                            })
                acc_mv_line_obj.create(cr, uid, {
                   'move_id': move_id,
                   'name': period_id.name + " Revaluation",
                   'account_id' : data[0],
                   'debit':  diff > 0 and diff or 0.0,
                   'credit': diff < 0 and -diff or 0.0, })
        if total :
            account_id = total > 0 and company_id.unreal_income_currency_exchange_account_id \
                or company_id.unreal_expense_currency_exchange_account_id
                
            if not account_id :
                raise osv.except_osv(_('Warning !'), _('Please define G/L currency exchance account in settings!'))
                
            acc_mv_line_obj.create(cr, uid, {
               'move_id': move_id,
               'name': period_id.name + " Revaluation",
               'account_id' : account_id.id,
               'analytic_account_id' : self.read(cr, uid, ids)[0]['analytic_account_id'][0],
               'debit':  total < 0 and -total or 0.0,
               'credit': total > 0 and total or 0.0, })
        return [move_id]
    
    def revaluate_currency(self, cr, uid, ids, context=None):
        res = self.revaluate(cr, uid, ids, context)
        if not res:
            raise osv.except_osv(_('Warning !'), _('There are no entries to revalue!'))
        
        if len(res) > 1 :
            return {
                'domain': "[('id','in',[" + ','.join(map(str, list(res))) + "])]",
                'name': _('Revaluation Entries'),
                'view_mode': 'tree,form',
                'view_type': 'form',
                'context': {'tree_view_ref': 'account.view_move_tree', 'form_view_ref': 'account.view_move_form'},
                'res_model': 'account.move',
                'type': 'ir.actions.act_window',
            }
        else :
            return {
                'res_id': res[0],
                'name': _('Revaluation Entries'),
                'view_mode': 'form',
                'view_type': 'form',
                'context': {'form_view_ref': 'account.view_move_form'},
                'res_model': 'account.move',
                'type': 'ir.actions.act_window',
            }
        
        
class account_bank_statement_line(osv.osv):
    _inherit = "account.bank.statement.line"

    def get_currency_rate_line(self, cr, uid, st_line, currency_diff, move_id, context=None):
        res = super(account_bank_statement_line, self).get_currency_rate_line(cr, uid, st_line, currency_diff, move_id, context)
        res['analytic_account_id'] = st_line.journal_id.company_id.realized_analytic_id.id
        return res