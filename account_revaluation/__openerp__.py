{
    "name": "Monthly Foreign Currency Revaluation",
    "version": "1.0",
    "depends": [
                'base', 
                'account',
                'account_move_reverse'
                ],
    "author":"PT. VISI",
    "category":"Accounting",
    "description" : """Monthly Foreign Currency Revaluation for Bank and AP transaction""",
    'data': [
        'account_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
