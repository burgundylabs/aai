from openerp.osv import fields, osv
from datetime import datetime
import openerp.addons.decimal_precision as dp
# ---------------------------------------------------------
# Budget Log : Dibutuhkan untuk mencatat mana yang sudah bisa dihitung sebagai Reserved Budget triggered by PO maupun Accounting
# ---------------------------------------------------------

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, workflow, api
import openerp.addons.decimal_precision as dp
  
class purchase_order(osv.osv):
    _inherit = 'purchase.order'
        
#     def extract_budget(self, cr, uid, ids, context=None):
#         #ngambil semua cost center yang ada pada purchase order line
#         super(purchase_order, self).wkf_confirm_order(cr, uid, ids, context)   
#         order_line=self.browse(cr,uid,ids).order_line
#         for line in order_line:
#             analytic = line.account_analytic_id
#         return True    
    
    def check_budget(self, cr, uid, cc, date_str, po_id, context=None):
        budget_line_obj = self.pool.get('brg.budget.line')
        # ngecek setiap cost center, apakah available budgetnya
        for key in cc:
            # A. cari planned budget terkait yang lagi running (validated) 
            cr.execute("SELECT bbl.id as id,bbl.planned_amount as pa \
                        FROM brg_budget_line bbl \
                        LEFT JOIN brg_budget bb on (bbl.budget_id=bb.id) \
                        WHERE bb.analytic_id = %s \
                        AND bbl.account_id = %s\
                        AND bb.date_from <= %s\
                        AND bb.date_to >= %s\
                        AND bb.state = 'validate'\
                        ", (cc[key][0][0], cc[key][0][1], date_str, date_str,))  # harus menggunakan ',' pada belakang variable date_str
            hasil_query = cr.dictfetchall()
            if hasil_query == []:
                name_cc = self.pool.get('account.analytic.account').browse(cr, uid, cc[key][0][0], context=None).name
                raise osv.except_osv(_('Warning !'), _('Budget for %s is not available in given time period, \n Please contact your Budget Supervisor.') % (name_cc))
            # Merapikan variable hasil query
            bbl_id = [hasil_query[0]['id']]
            planned_cc = hasil_query[0]['pa']
            
            # Cari Reservasi dari tanggal segitu dan status log nya validate
#             date_from = budget_line_obj.browse(cr, uid, bbl_id).budget_id.date_from
#             date_to = budget_line_obj.browse(cr, uid, bbl_id).budget_id.date_to
#             cr.execute("SELECT sum(amount) \
#                         FROM brg_budget_log \
#                         WHERE analytic_id = %s \
#                         AND account_id = %s \
#                         AND date >= %s\
#                         AND date <= %s\
#                         AND state = 'validate'\
#                         ", (cc[key][0][0], cc[key][0][1], date_from, date_to,))  # harus menggunakan ',' pada belakang variable date_str
#             
#             hasil_query = cr.dictfetchall()
#             resv_cc = hasil_query[0]['sum']
#             if resv_cc is None:
#                 resv_cc = 0
            # Remaining
#             remaining = planned_cc - resv_cc

            #tambahan anif
            bbl_analytic_rec = budget_line_obj.browse(cr, uid, bbl_id).budget_id.analytic_id
            bbl_account_rec = budget_line_obj.browse(cr, uid, bbl_id).account_id
            bbl_fiscalyear_rec = budget_line_obj.browse(cr, uid, bbl_id).fiscalyear_id
            
            dom=[('account_id', '=', bbl_account_rec.id), ('fiscalyear_id', '=', bbl_fiscalyear_rec.id)]
            line_ids = self.pool.get('brg.budget.line').search(cr, uid, dom)
            bbl_line_ids = self.pool.get('brg.budget.line').browse(cr, uid, line_ids)
            
            total_account_budget = 0
            for bbl_line_id in bbl_line_ids:
                if (bbl_line_id.budget_id.analytic_id.id == bbl_analytic_rec.id) and (bbl_line_id.state=='validate'):
                    total_account_budget += bbl_line_id.planned_amount - bbl_line_id.res_amount         
                else:
                    continue
            if cc[key][1] > total_account_budget:
                # Mencari gr_budget_spv ID
                model_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'brg_fin_sec', 'gr_budget_spv')
                group_id = model_id and model_id[1]
                
                user_ids = self.pool.get('res.users').search(cr, uid, [('groups_id', 'in', group_id)])
                partner_ids = [self.pool.get('res.users').browse(cr, uid, user_id).partner_id.id for user_id in user_ids]
                # Add All Trs Spv to Follow
                self.message_subscribe(cr, uid, [po_id], partner_ids, context=context)
                # Send Message    
                message_obj = self.pool.get('mail.message')
                vals = {'body': '<p>This Purchase Order is insufficient budget.</p>',
                        'model': 'purchase.order',
                        'res_id': po_id,
                        'subtype_id': 1,
                        'author_id': uid,
                        'type': 'comment',
                }
                message_obj.create(cr, uid, vals, context)     
#                 import ipdb;ipdb.set_trace();           
                name_cc = self.pool.get('account.analytic.account').browse(cr, uid, cc[key][0][0], context=None).name

                # Membuat warning dgn wizard
#                     result = self.pool.get('warning.wizard').wizard_view(cr, uid, ids, context)
#                     import ipdb;ipdb.set_trace();
#                     result = {
#                         'name': _('Warning'),
#                         'view_type':'form',
#                         'view_mode':'form',
#                         'res_model': 'warning.wizard',
#                         'type':'ir.actions.act_window',
#                         'target': 'new',
#                         'context': {'form_view_ref': 'brg_budget_control.warning_wizard'}
#                     }
                raise osv.except_osv(_('Warning !'), _('Insufficient Budget for %s in given time period, \n Please contact your Budget Supervisor.') % (name_cc))
            # Merapikan variable hasil query
            else:
                result = True
        return result
    
    def wkf_confirm_order(self, cr, uid, ids, context=None):
        for order in self.browse(cr, uid, ids):
            date = self.browse(cr, uid, order.id).date_order
            # ngambil semua cost center yang ada pada purchase order line
            cc = {}  # kumpulan cost center dan amount
            
            # get currency
            def_cur_id = self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.currency_id
            order_cur_id = self.browse(cr, uid, order.id, context=None).currency_id
            rate = 1
            if def_cur_id != order_cur_id:
                cur_obj = self.pool.get('res.currency')
                rate = cur_obj._get_conversion_rate(cr, uid, order_cur_id, def_cur_id, context=None)
            
            for line in order.order_line:
                sel_cc = line.account_analytic_id.id  # sel_cc = selected Cost Center
                sel_aa = line.product_id.property_account_expense.id  or line.product_id.categ_id.property_account_expense_categ.id  # sel_aa = selected Account
                if not sel_aa:
                    raise osv.except_osv(('Warning!'), ('Expense Account is not set in product or product category.')) 
                     
                line_amount = line.price_subtotal * rate  # sel_cc = line amount * Rate
                found = False
                if cc:
                    for i in cc:
                        if [sel_cc, sel_aa] == cc[i][0]:
                            cc.update({i: [cc[i][0], cc[i][1] + line_amount]})
                            found = True
                    if not found:
                        cc[line.id] = [sel_cc, sel_aa], line_amount
                else:
                    cc[line.id] = [sel_cc, sel_aa], line_amount
                    
            date_obj = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            date_str = date_obj.strftime('%Y-%m-%d')
            
            # Cek reserved budget
            result = self.check_budget(cr, uid, cc, date_str, order.id, context=None)
            if isinstance(result, dict):
                return result
            
            # Setelah benar semua budget OK baru di Log
            order_name = self.browse(cr, uid, ids).name
            po_id = self.browse(cr, uid, ids).id
            # import ipdb;ipdb.set_trace();
            for cc_id in cc:
                budget_log_obj = self.pool.get('brg.budget.log')
                budget_log_obj.create(cr, uid, {
                                              'analytic_id':cc[cc_id][0][0],
                                              'account_id':cc[cc_id][0][1],
                                              'amount':cc[cc_id][1],
                                              'date':date_str,
                                              'desc':order_name,
                                              'po_id':po_id,
                                              'rate':rate,
                                              'currency_id':order_cur_id.id,
                                              }, context=None)
             
        return super(purchase_order, self).wkf_confirm_order(cr, uid, ids, context)  
    
    def wkf_action_cancel(self, cr, uid, ids, context=None):
        super(purchase_order, self).wkf_action_cancel(cr, uid, ids, context)
        budget_log_obj = self.pool.get('brg.budget.log')
        tuple_id = []
        for sel_id in ids:
            tuple_id.append(sel_id)
        # import ipdb;ipdb.set_trace();
        cr.execute("DELETE \
                        FROM brg_budget_log \
                        WHERE po_id in %s \
                        ", (tuple(tuple_id),))  # harus menggunakan ',' pada belakang variable date_str
            
            
        
        return True
    
    

class purchase_order_line(osv.osv):
    _inherit = 'purchase.order.line'
    _columns = {
        'account_analytic_id':fields.many2one('account.analytic.account', 'Analytic Account', required=True),

    }

class purchase_requisition_line(osv.osv):
    _inherit = 'purchase.requisition.line'
    _columns = {
        'account_analytic_id':fields.many2one('account.analytic.account', 'Analytic Account', required=True),

    }