from openerp.osv import fields, osv
from openerp.tools.translate import _
# ---------------------------------------------------------
# Budgets
# ---------------------------------------------------------
class brg_budget(osv.osv_memory):
    _name = "warning.wizard"
    _description = "Warning!"
        
    def wizard_view(self, cr, uid, ids, context):
        view = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'brg_budget_control', 'warning_wizard')
#         import ipdb;ipdb.set_trace();
        return {
            'name': _('Warning!'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'warning.wizard',
            'views': [(view[1], 'form')],
            'view_id': view[1],
            'target': 'new',
            'res_id': ids[0],
            'context': context,
        }