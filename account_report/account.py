from openerp import models, fields, api, _
from openerp.osv import osv, fields
import time
from openerp.exceptions import except_orm, Warning, RedirectWarning
    
class account_move_line(osv.Model):
    _inherit = "account.move.line"
    
    def _rate(self, cr, uid, ids, name, arg, context):
        res = {}
        for line in self.browse(cr,uid,ids):
            if line.currency_id:
                amount = (line.debit or line.credit) / line.amount_currency 
            else:
                amount = 1.0
                
            res[line.id] = abs(amount)
        return res
    
    _columns = {"rate": fields.function(_rate, string="Rate"),
    }
    
class account_invoice(osv.Model):
    _inherit = "account.invoice"
    
    _columns = {'opening_words': fields.text("Opening Words"),
#                 'description': fields.text("Description"),
#                 'form_no': fields.char("Form Number"),
                'rate': fields.float("Rate", readonly=True, copy=False),
                'fob': fields.char("FOB"),
                'ship_via': fields.char("Ship Via"),
                'ship_date': fields.date("Ship Date"),
    }
    
    @api.multi
    def action_move_create(self):
        super(account_invoice, self).action_move_create()
        for inv in self:
            if inv.currency_id != inv.company_id.currency_id:
                currency = self.currency_id.with_context(date=self.date_invoice or fields.Date.context_today(self))
                rate_amount = currency._get_conversion_rate(from_currency=inv.currency_id, to_currency=inv.company_id.currency_id)
                inv.rate = rate_amount
            else:
                inv.rate = 1.0
        return True
    
class account_bank_statement(osv.Model):
    _inherit = "account.bank.statement"
    
    def _sum_idr(self, cr, uid, ids, name, arg, context):
        res = {}
        for bank in self.browse(cr,uid,ids):
            sum_idr = 0.0
            sum_valas = 0.0
            for line in bank.line_ids:
                sum_idr += line.amount
                sum_valas += line.amount_currency
            
            res[bank.id] = {'sum_idr': abs(sum_idr),
                            'sum_valas': abs(sum_valas),
                            }
        return res
    
    _columns = {"sum_idr": fields.function(_sum_idr, string="Total IDR", multi="sam"),
                "sum_valas": fields.function(_sum_idr, string="Total IDR", multi="sam"),
    }
    
class account_bank_statement_line(osv.Model):
    _inherit = "account.bank.statement.line"
    
    def _absolute(self,cr,uid,ids,name,arg,context):
        res={}
        
        for bank_line in self.browse(cr,uid,ids):
            #import ipdb;ipdb.set_trace();
            res[bank_line.id] = {'abs_idr': abs(bank_line.amount),
                                 'abs_valas': abs(bank_line.amount_currency),
                                 }
        return res
            
    _columns = {"abs_idr": fields.function(_absolute, string="Amount", multi="sum"),
                "abs_valas": fields.function(_absolute, string="Amount", multi="sum"),
    }
    
class stock_picking(osv.osv):
    _inherit = "stock.picking"

    def _create_invoice_from_picking(self, cr, uid, picking, vals, context=None):
        vals['ship_date'] = picking.date_done
        return super(stock_picking, self)._create_invoice_from_picking(cr, uid, picking, vals, context)


