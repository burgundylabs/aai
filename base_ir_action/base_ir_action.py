import time

from openerp import SUPERUSER_ID
from openerp.osv import osv, fields
from openerp.osv import fields, osv, expression

class ir_actions_act_window_burgundy(osv.Model):

    _inherit = 'ir.actions.act_window'
    
    def _eval_context(self, cr, uid):
        """Returns a dictionary to use as evaluation context for
           ir.rule domains."""
        return {'user': self.pool.get('res.users').browse(cr, SUPERUSER_ID, uid),
                'time':time}    
                
    def _get_domain(self, cursor, user, ids, name, arg, context=None):
        # import ipdb;ipdb.set_trace()
        res = {}
        eval_context= self._eval_context(cursor,user)

        for act_window in self.browse(cursor, user, ids, context=context):
            if act_window.domain_force:
                if 'domain_eval' in act_window.domain_force:            
                    #import ipdb;ipdb.set_trace()
                    eval_domain = act_window.domain_force
                    new = eval_domain.replace("domain_eval", "")
                    res[act_window.id] = expression.normalize_domain(eval(new, eval_context))
                else:
                    res[act_window.id] = act_window.domain_force
            else:
                res[act_window.id]=False
        #import ipdb;ipdb.set_trace()
        return res
    
    def _save_domain(self, cr, uid, id, field, value, arg, context=None):
        #import ipdb;ipdb.set_trace()
        if value:
            self.write(cr, uid, id, {'domain_force': value}, context=context)
        return value
            
    _columns = {
        'domain_force':fields.char("Domain Temp"),
        
        'domain':fields.function(_get_domain, string='Domain Force',fnct_inv=_save_domain, type='text', help="Menghitung Jumlah Total Kelas"),
    }