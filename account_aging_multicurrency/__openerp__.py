{
    "name": "Aging Multi Currency",
    "version": "1.0",
    "depends": ['account',
                'account_accountant'],
    "author":"PT. VISI",
    "category":"Accounting and Finance",
    "description" : """Report for Partner Aging in Multi Currency""",
    'data': [
        'views/report_agedpartnerbalance.xml',
        'wizard/account_report_aged_partner_balance_view.xml',
        'account_report.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
