from openerp.osv import osv, fields

class purchase_order_done(osv.Model):
    
    
    def purchase_done(self, cr, uid, ids, context=None):
        #import ipdb;ipdb.set_trace();
        self.write(cr,uid,ids,{'state':'done'},context=None)
        return True
    
    _inherit = "purchase.order"
