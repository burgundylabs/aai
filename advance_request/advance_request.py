from openerp.osv import osv, fields
from openerp.tools.translate import _
from datetime import datetime, timedelta
import openerp.addons.decimal_precision as dp

    
class res_partner(osv.osv):
    _inherit = 'res.partner'
    
    _columns = {
    'property_account_receivable': fields.property(
            type='many2one',
            relation='account.account',
            string="Account Receivable",
            domain="[('type', 'in', ('payable','receivable'))]",
            help="This account will be used instead of the default one as the receivable account for the current partner",
            required=True),
    }
    
class res_company(osv.osv):
    _inherit = 'res.company'
    
    _columns = {
        'advance_account_id': fields.many2one('account.account', "Default Advance Account"),
        'advance_journal_id': fields.many2one('account.journal', "Default Advance Journal"),
    }
    
class account_config_settings(osv.osv):
    _inherit = 'account.config.settings'
     
    _columns = {
        'advance_account_id': fields.related(
            'company_id', 'advance_account_id',
            type='many2one',
            relation='account.account',
            string="Default Advance Account",),
        'advance_journal_id': fields.related(
            'company_id', 'advance_journal_id',
            type='many2one',
            relation='account.journal',
            string="Default Advance Journal",),
    }
    
    def onchange_company_id(self, cr, uid, ids, company_id, context=None):
        res = super(account_config_settings, self).onchange_company_id(cr, uid, ids, company_id, context=context)
        if company_id:
            company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
            res['value'].update({
                'advance_account_id': (company.advance_account_id
                    and company.advance_account_id.id or False),
                'advance_journal_id': (company.advance_journal_id
                    and company.advance_journal_id.id or False),
                })
        else: 
            res['value'].update({
                'advance_account_id': False,
                'advance_journal_id': False,
                })
        return res
     
class advance_request(osv.Model):
    _name = "advance.req"
    _description = "Advance Request"
    _inherit = 'mail.thread'
    _order = "date desc, name desc"
    
    def _get_account(self, cr, uid, context=None):
        company_obj = self.pool.get('res.company')
        user_obj = self.pool.get('res.users')
        company_id = user_obj.browse(cr, uid, uid, context=context).company_id.id
        account_id = company_obj.browse(cr, uid, company_id).advance_account_id.id
        return account_id
    
    def _get_journal(self, cr, uid, context=None):
        company_obj = self.pool.get('res.company')
        user_obj = self.pool.get('res.users')
        company_id = user_obj.browse(cr, uid, uid, context=context).company_id.id
        journal_id = company_obj.browse(cr, uid, company_id).advance_journal_id.id
        return journal_id
    
    _columns = {
        "name": fields.char("Name", readonly=True),
        "user_id": fields.many2one("res.users", "Created by", readonly=True, domain="[('is_company', '!=', 'True')]"),
        "partner_id": fields.many2one("res.partner", "Employee", readonly=True, required=True, states={'draft': [('readonly', False)]}),
        "company_id": fields.many2one("res.company", "Company", readonly=True),
        "account_id": fields.many2one("account.account", "Advance Account", readonly=True, required=True, states={'draft': [('readonly', False)]}, domain="[('type', '!=', 'view')]"),
        "date": fields.date("Date", readonly=True, required=True, states={'draft': [('readonly', False)]}),
        "description": fields.char("Description", readonly=True, required=True, states={'draft': [('readonly', False)]}),
        "currency_id": fields.many2one('res.currency', 'Currency'),  # nanti diisi mata uang terkait
        'move_id': fields.many2one('account.move', 'Journal Entry', readonly=True, copy=False),
        'stl_move_id': fields.many2one('account.move', 'Settlement Journal Entry', readonly=True, copy=False),
        'pay_move_id': fields.many2one('account.move', 'Payment/Refund Journal Entry', readonly=True, copy=False),
        "amount": fields.float("Amount", readonly=True, required=True, states={'draft': [('readonly', False)]}),
        "actual_amount": fields.float("Actual Amount", readonly=True),
        "state": fields.selection([("draft", "Draft"),
                                   ("done", "Confirmed"),
                                   ("settled", "Settled"),
                                   ("cancelled", "Cancelled"), ], "Status"),
        'move_line_ids': fields.related('move_id', 'line_id', type='one2many', relation='account.move.line', string='Journal Items', readonly=True),
        'stl_move_line_ids': fields.related('stl_move_id', 'line_id', type='one2many', relation='account.move.line', string='Settlement', readonly=True),
        'pay_move_line_ids': fields.related('pay_move_id', 'line_id', type='one2many', relation='account.move.line', string='Payment/Refund', readonly=True),
    }
    
    _defaults = {
        'date': fields.datetime.now,
        "state": "draft",
        "user_id": lambda obj, cr, uid, context: uid,
        'currency_id': lambda self, cr, uid, context: self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.currency_id.id,
        "company_id": lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'advance.req', context=c),
        "account_id": _get_account,
        "journal_id": _get_journal,
    }
    
    _order = 'date desc, id desc'
    
    def action_cancel(self, cr, uid, ids, context=None):
        ctx = context or {}
        for adv in self.browse(cr, uid, ids):
            adv.state = "cancelled"
            self.message_post(cr, uid, [adv.id], body=_("Advance Request Cancelled"), context=ctx)
            
            # Set all line to cancelled state
#             for line in adv.req_line_ids:
#                 line.state = 'cancelled'
                
        return True
    def create(self, cr, uid, vals, context):
        ctx = context or {}
        # Set Name
        vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'advance.req') or '/'         
        new_id = super(advance_request, self).create(cr, uid, vals, context=ctx)
        return new_id
        
class advance_request_line(osv.Model):
    _name = "advance.req.line"
    _columns = {
        "debit": fields.float("Debit", digits_compute=dp.get_precision('Account'), states={'draft': [('readonly', False)]}),
        "credit": fields.float("Credit", digits_compute=dp.get_precision('Account'), states={'draft': [('readonly', False)]}),
        "description": fields.text("Description", states={'draft': [('readonly', False)]}),
        "advance_id": fields.many2one("advance.req", "Advance Request"),
        "account_id": fields.many2one("account.account", "Account"),
        "state": fields.selection([("draft", "Draft"),
                                   ("done", "Done"),
                                   ("cancelled", "Cancelled"), ], "Status"),
    }
    
    _defaults = {
        "state": "draft",
    }
