{
    'name' : 'Advance Request AAI',
    'version' : '1.1',
    'author' : 'PT. VISI',
    'category' : 'Accounting & Finance',
    'description' : """Advance Request AAI""",
    'website': 'https://www.visi.co.id',
    'depends' : ['account',
                 'brg_fin_sec',
                 ],
    'data': [
             "wizard/advance_settlement_view.xml",
             "wizard/advance_confirm_view.xml",
             "advance_request_view.xml",
             "advance_report.xml",
             "adv_sequence.xml",
             "views/report_advrequest.xml",
             "security/ir.model.access.csv",
    ],
   
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
