import time

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class advance_settlement(osv.osv_memory):
    _name = "advance.settlement"
    _description = "Advance Settlement"
    _columns = {
        'date': fields.date('Date'),
        'journal_id': fields.many2one('account.journal', 'Payment Method', domain="[('type', 'in', ('cash','bank'))]"),
        'settlement_line_ids': fields.one2many('advance.settlement.line', 'settlement_id', 'Settlement Line'),
    }

    def _get_amount(self, cr, uid, context=None):
#         import ipdb;ipdb.set_trace();
        adv_obj = self.pool.get('advance.req')
        adv = adv_obj.browse(cr, uid, context['active_id'])
        return adv.amount

    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d'),
        'amount': _get_amount,
    }

    def action_settlement(self, cr, uid, ids, context=None):
        adv_obj = self.pool.get('advance.req')
        adv = adv_obj.browse(cr, uid, context['active_id'])
        aml_obj = self.pool.get('account.move.line')
        for wiz in self.browse(cr, uid, ids):
            total_amount = 0.0
            aml_to_reconcile = []
            # Membuat journal entry
            period_id = self.pool.get('account.period').find(cr, uid, wiz.date, context)
            vals = {'journal_id': adv.company_id.advance_journal_id.id,
                    'period_id': period_id[0],
                    'ref': adv.name,
                    'date': wiz.date,
            }
            move_id = self.pool.get('account.move').create(cr, uid, vals)
            
            # Membuat Journal Item
            for line in wiz.settlement_line_ids:
                # Line Debit
                vals_item = {'name': line.name,
                             'partner_id': adv.partner_id.id,
                             'account_id': line.account_id.id,
                             'debit': line.amount,
                             'credit': 0.0,
                             'analytic_account_id': line.analytic_account_id.id,
                             'move_id': move_id,
                             }
                self.pool.get('account.move.line').create(cr, uid, vals_item)
                # Hitung Total Amount
                total_amount += line.amount
                
            # Line Credit
            vals_item = {'name': adv.description,
                         'partner_id': adv.partner_id.id,
                         'account_id': adv.account_id.id,
                         'credit': total_amount,
                         'debit': 0.0,
                         'move_id': move_id,
                         }
            aml_to_reconcile.append(self.pool.get('account.move.line').create(cr, uid, vals_item))
            # Post Journal Entry
            self.pool.get('account.move').button_validate(cr, uid, move_id, context)
            
            dif_amount = total_amount - adv.amount
            # Jika ada selisih
            if dif_amount:
                # Membuat journal entry
                period_id = self.pool.get('account.period').find(cr, uid, wiz.date, context)[0]
                vals = {'journal_id': wiz.journal_id.id,
                        'period_id': period_id,
                        'ref': adv.name + " (Settlement)",
                        'date': wiz.date,
                }
                pay_move_id = self.pool.get('account.move').create(cr, uid, vals)
                # Jika aktual expense lebih besar
                if dif_amount > 0:
                    # Debit Line
                    vals_item = {'name': adv.description + " (Settlement)",
                                 'partner_id': adv.partner_id.id,
                                 'account_id': adv.account_id.id,
                                 'debit': abs(dif_amount),
                                 'credit': 0.0,
                                 'move_id': pay_move_id,
                                 }
                    self.pool.get('account.move.line').create(cr, uid, vals_item)
                    # Credit Line
                    vals_item = {'name': adv.description + " (Settlement)",
                                 'partner_id': adv.partner_id.id,
                                 'account_id': wiz.journal_id.default_credit_account_id.id,
                                 'credit': abs(dif_amount),
                                 'debit': 0.0, 
                                 'move_id': pay_move_id,
                                 }
                    aml_to_reconcile.append(self.pool.get('account.move.line').create(cr, uid, vals_item))
                # Jika aktual expense lebih kecil
                elif dif_amount < 0:
                    # Debit Line
                    vals_item = {'name': adv.description + " (Settlement)",
                                 'partner_id': adv.partner_id.id,
                                 'account_id': wiz.journal_id.default_debit_account_id.id,
                                 'debit': abs(dif_amount),
                                 'credit': 0.0,
                                 'move_id': pay_move_id,
                                 }
                    self.pool.get('account.move.line').create(cr, uid, vals_item)
                    # Credit Line
                    vals_item = {'name': adv.description + " (Settlement)",
                                 'partner_id': adv.partner_id.id,
                                 'account_id': adv.account_id.id,
                                 'credit': abs(dif_amount),
                                 'debit': 0.0,
                                 'move_id': pay_move_id,
                                 }
                    aml_to_reconcile.append(self.pool.get('account.move.line').create(cr, uid, vals_item))
                # Post Journal Entry
                self.pool.get('account.move').button_validate(cr, uid, pay_move_id, context)
                adv.pay_move_id = pay_move_id
            
            # Reconcile
            aml_to_reconcile.append(aml_obj.search(cr, uid, ('&', ['move_id', '=', adv.move_id.id], ['account_id', '=', adv.account_id.id]))[0])
            aml_obj.reconcile_partial(cr, uid, aml_to_reconcile, context=context)
            
            adv.stl_move_id = move_id
            adv.actual_amount = total_amount
            adv.state = 'settled'
#                 import ipdb;ipdb.set_trace();
            self.pool.get(context['active_model']).message_post(cr, uid, [adv.id], body=_("Advance Request Settled"), context=context)
            
        return True

class advance_settlement_line(osv.osv_memory):
    _name = "advance.settlement.line"
    _description = "Advance Settlement Line"
    
    _columns = {
        'settlement_id': fields.many2one('advance.settlement', 'Settlement'),
        'name': fields.char('Description', required=True),
        'account_id': fields.many2one('account.account', 'Account', required=True, domain="[('type', 'not in', ('view','liquidity'))]"),
        'analytic_account_id': fields.many2one("account.analytic.account", "Analytic Account", domain="[('type', '!=', 'view')]"),
        'amount': fields.float('Amount', digits_compute=dp.get_precision('Account'), required=True),
    }
    
    _defaults = {  
        'amount': 0.0,
        }
    
    
