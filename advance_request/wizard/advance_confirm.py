import time

from openerp.osv import fields, osv
from openerp.tools.translate import _

class advance_confirm(osv.osv_memory):
    _name = "advance.confirm"
    _description = "Advance Confirm"
    
    _columns = {
       'journal_id': fields.many2one('account.journal', 'Payment Method', required=True, domain="[('type', 'in', ('cash','bank'))]"),
       'date': fields.date('Entry Date'),
       'due_date': fields.date("Due Date", required=True),
    }
    
    def action_confirm(self, cr, uid, ids, context=None):
        ctx = context or {}
        adv = self.pool.get('advance.req').browse(cr, uid, ctx.get('active_id', False))
            
        for wiz in self.browse(cr,uid,ids):
        
            # Membuat journal entry
            period_id = self.pool.get('account.period').find(cr, uid, adv.date, context)
            vals = {'journal_id': wiz.journal_id.id,
                    'period_id': period_id[0],
                    'ref': adv.name,
                    'date': wiz.date or adv.date,
            }
            move_id = self.pool.get('account.move').create(cr, uid, vals)
            
            # Membuat Journal Item
            # Line Debit
            vals_item = {'name': adv.description,
                         'partner_id': adv.partner_id.id,
                         'account_id': adv.account_id.id,
                         'debit': adv.amount,
                         'credit': 0.0,
    #                          'analytic_account_id': adv.analytic_account_id.id,
                         'move_id': move_id,
                         'date_maturity': wiz.due_date,
                         }
            self.pool.get('account.move.line').create(cr, uid, vals_item)
            
            # Line Credit
            vals_item = {'name': adv.description,
                         'partner_id': adv.partner_id.id,
                         'account_id': wiz.journal_id.default_credit_account_id.id,
                         'credit': adv.amount,
                         'debit': 0.0,
    #                          'analytic_account_id': adv.analytic_account_id.id,
                         'move_id': move_id,
                         }
            self.pool.get('account.move.line').create(cr, uid, vals_item)
            
            # Post Journal Entry
            self.pool.get('account.move').button_validate(cr, uid, move_id, context)
            
            # Mencari Treasury Spv & Accounting Spv ID
#             model_ids = self.pool.get('ir.model.data').search(cr, uid, [('model', '=', 'res.groups'), ('module', '=', 'brg_fin_sec'), ('name', 'in', ('gr_acc_spv', 'gr_tre_spv'))])
    
#             group_ids = []
#             for model_id in model_ids:
#                 group_ids.append(self.pool.get('ir.model.data').browse(cr, uid, model_id).res_id)
#             
#             user_ids = self.pool.get('res.users').search(cr, uid, [('groups_id', 'in', group_ids)])
#             
#             partner_ids = [self.pool.get('res.users').browse(cr, uid, user_id).partner_id.id for user_id in user_ids]
    
            # Add All Trs Spv to Follow
#             self.pool.get('advance.req').message_subscribe(cr, uid, [adv.id], partner_ids, context=context)
            
            # Send Message to All Treasury Spv & Accounting Spv
#             message_obj = self.pool.get('mail.message')
#             vals = {'body': '<p>This Advance Request has been approved. Please follow it up. Thanks you.</p>',
#                     'model': 'advance.req',
#                     'res_id': adv.id,
#                     'subtype_id': 1,
#                     'author_id': uid,
#                     'type': 'comment',
#                     }
#             message_obj.create(cr, uid, vals, context)
            
            # Set done
            adv.state = "done"
            self.pool.get('advance.req').message_post(cr, uid, [adv.id], body=_("Advance Request Confirmed"), context=ctx)
            adv.move_id = move_id
            
            # Set all line to done state
    #             for line in adv.req_line_ids:
    #                 line.state = 'done'
                
        return True
