from openerp.osv import osv, fields

class res_users_cc(osv.Model):
    _inherit = "res.users"
    _columns = {
        'cc_id': fields.many2one('account.analytic.account', 'Cost Center', domain=[('type','=','normal')]), #nanti diisi cost Center
    }

class analytic_account_cc(osv.osv):
    _inherit = "account.analytic.account"
    _columns = {
        'user_ids': fields.one2many('res.users', 'cc_id', 'User'), 
    }
