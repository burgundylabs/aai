from openerp.osv import osv, fields
import time


class stock_picking(osv.Model):
    _inherit = "stock.picking"
    
    _columns = {
        'supplier_ref': fields.char("Supplier Reference"),
        'date_received': fields.date("Received Date"),
    }
    
    def _get_invoice_vals(self, cr, uid, key, inv_type, journal_id, move, context=None):
        res = super(stock_picking, self)._get_invoice_vals(cr, uid, key, inv_type, journal_id, move, context)
        if res.get('origin', False):
            if move.purchase_line_id:
                res['origin'] = move.purchase_line_id.order_id.name + '-' + move.picking_id.name
        return res

class stock_move(osv.Model):
    _inherit = "stock.move"
    _order = 'date_expected, id'