from openerp.osv import osv, fields
import time
from openerp.tools.translate import _
    
class account_bank_statement(osv.Model):
    _inherit = "account.bank.statement"
    
    def _compute_default_statement_name(self, cr, uid, journal_id, context=None):
        '''Nomor Internal Memo tidak menggunakan nomor journal payment'''
        context = dict(context or {})
        name = self.pool.get('ir.sequence').get(cr, uid, 'internal.memo') or '/'
        return name
    
class account_move_line(osv.Model):
    _inherit = "account.move.line"
    
    _order = 'debit desc'
    
    def name_get(self, cr, uid, ids, context=None):
        '''Menambahkan nama journal item dengan nama supplier invoice number'''
        if not ids:
            return []
        result = []
        for line in self.browse(cr, uid, ids, context=context):
#             import ipdb;ipdb.set_trace();
            if line.ref:
                result.append((line.id, (line.move_id.name or '')+' ('+line.name+')'))
            else:
                result.append((line.id, line.move_id.name))
        return result