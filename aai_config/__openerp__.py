{
    "name": "AAI Configuration",
    "version": "1.0",
    "depends": ['base',
                'account_voucher',
                'report',
                'account_cancel'],
    "author": "PT. VISI",
    "category": "Setting",
    "description" : """AAI Configuration""",
    'data': [
        'views/layouts.xml',
        'account_view.xml',
        'stock_view.xml',
        'internal_memo_sequence.xml',
],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
