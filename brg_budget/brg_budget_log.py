from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp

class brg_budget_log(osv.Model):
        
    def validate_log(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {
            'state': 'validate'
        })
        return True
    def cancel_log(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {
            'state': 'cancel'
        })
        return True
    def delete_log(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {
            'state': 'delete'
        })
        return True
    
    def unlink(self, cr, uid, ids, context=None):
        raise osv.except_osv(('Invalid Action!'), ("You cannot delete Budget Log. However you may change the state to 'Delete'"))
        return super(brg_budget_log, self).unlink(cr, uid, ids, context=context)
       
    _name = "brg.budget.log"
    _rec_name = "amount"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Budget Log"
         
    STATE_SELECTION = [
        ('delete', 'Deleted'),
        ('validate', 'Validated'),
        ('cancel', 'Cancelled')
    ]   
    _columns = {
        'account_id': fields.many2one('account.account', 'Account',track_visibility='onchange'), 
        'analytic_id': fields.many2one('account.analytic.account', 'Cost Center',track_visibility='onchange'),
        'currency_id': fields.many2one('res.currency', 'Currency',track_visibility='onchange'),
        'rate':fields.float('Rate',track_visibility='onchange'),
        'amount':fields.float('Amount',track_visibility='onchange'),
        'date':fields.date('Date',track_visibility='onchange'), #untuk kemudahan mencari dari budget
        'desc':fields.text('Description',track_visibility='onchange'), #untuk kemudahan mencari dari budget
        #kolom relasi ke PO LINE
        #kolom relasi ke Invoice LIne
        'po_id': fields.many2one('purchase.order', 'Related PO',track_visibility='onchange'), 
        'ir_id': fields.many2one('account.invoice', 'Related IR',track_visibility='onchange'), 
        'state':fields.selection(STATE_SELECTION,"Status",track_visibility='onchange'),
    }
    _defaults = {
        'state':"validate",
    }
