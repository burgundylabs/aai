{
    "name":"AAI - Budget",
    'author' : 'PT. VISI',
    "installable":True,
    "depends":[
            'account',
            'purchase',
            'brg_fin_sec',
            'res_users_cc',
#             'base_ir_action',
    ],
    "data":[
            'brg_budget_view.xml',
            'brg_budget_log_view.xml',
            'security/ir.model.access.csv',
            'wizard/brg_budget_wizard_view.xml',
            'report/report_budget_view.xml'
            
    ],
    
    
}