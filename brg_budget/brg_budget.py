from openerp.osv import fields, osv
from openerp import api, models, fields as Fields
import openerp.addons.decimal_precision as dp
# ---------------------------------------------------------
# Budgets
# ---------------------------------------------------------
class brg_budget(osv.Model):
    
    def budget_confirm(self, cr, uid, ids, *args):
        # import ipdb;ipdb.set_trace();
        cc_id = self.browse(cr, uid, ids, context=None).analytic_id.id
        cc_to_write = False;
        if cc_id:
            cc_to_write = cc_id;
        if cc_id == False:
            users_obj = self.pool.get('res.users')
            cc_to_write = self.browse(cr, uid, ids, context=None).user_id.cc_id.id
        
        self.write(cr, uid, ids, {
            'state': 'confirm',
            'analytic_id': cc_to_write,
        })
        # add by anf
        brg_obj = self.browse(cr,uid,ids)
        for line in self.browse(cr, uid, ids).line_ids:
            line.write({'state': 'confirm', 'date_from': brg_obj.date_from, 'date_to': brg_obj.date_to, 'fiscalyear_id': brg_obj.fiscalyear_id.id})
        # ##
        return True

    def budget_draft(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {
            'state': 'draft'
        })
        # add by anf
        for line in self.browse(cr, uid, ids).line_ids:
            line.write({'state': 'draft'})
        # ##
        return True
    
    def budget_suspend(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {
            'state': 'suspend'
        })
        # add by anf
        for line in self.browse(cr, uid, ids).line_ids:
            line.write({'state': 'suspend'})
        # ## 
        return True    
    def budget_rerun(self, cr, uid, ids, *args):
        obj_bgt = self.browse(cr, uid, ids, context=None)
        total_budget_year = self.pool.get('brg.budget.line').check_total_budget_year(cr, uid, obj_bgt.fiscalyear_id.id)
        if total_budget_year < obj_bgt.fiscalyear_id.total_budget:
            self.write(cr, uid, ids, {
                                        'amount_max': obj_bgt.amount_total,
                                        'state': 'validate'
                                        })
            # add by anf
            for line in self.browse(cr, uid, ids).line_ids:
                line.write({'state': 'validate'})
            # ## 
        else:
            raise osv.except_osv(('Error !'), ('Budget amount cannot exceeds the total fiscal year budget.')) 
        return True

    def budget_validate(self, cr, uid, ids, *args):
        # import ipdb;ipdb.set_trace();
        obj_bgt = self.browse(cr, uid, ids, context=None)
        analytic_id = obj_bgt.analytic_id.id
        if analytic_id is False:
            raise osv.except_osv(('Warning !'), ('Cost center needs to be defined')) 
        # A. cari planned budget yang lagi running(validated) terkait
        filter1 = self.search(cr, uid, [('date_to', '>=', obj_bgt.date_from), ('date_from', '<=', obj_bgt.date_to), ('analytic_id', '=', obj_bgt.analytic_id.id)], context=None)
        filter2 = self.search(cr, uid, ['&', ('state', 'in', ('validate', 'suspend', 'done')), ('id', 'in', filter1), ], context=None)
        
        if filter2:
            raise osv.except_osv(('Error !'), ('There is already running/suspended budget on the given period of time'))  
        #tambahan anif
        total_budget_year = self.pool.get('brg.budget.line').check_total_budget_year(cr, uid, obj_bgt.fiscalyear_id.id)
        
#         if (obj_bgt.amount_max == 0) or (obj_bgt.amount_max >= obj_bgt.amount_total):
        if total_budget_year < obj_bgt.fiscalyear_id.total_budget:
            self.write(cr, uid, ids, {
                                        'amount_max': obj_bgt.amount_total,
                                        'state': 'validate',
                                        'validating_user_id': uid,
                                        })       
            # add by anf
            for line in self.browse(cr, uid, ids).line_ids:
                line.write({'state': 'validate'})
            # ## 
        else:
            raise osv.except_osv(('Error !'), ('Budget amount cannot exceeds the total fiscal year budget.')) 
        return True

    def budget_cancel(self, cr, uid, ids, *args):
        self.write(cr, uid, ids, {
            'state': 'cancel'
        })
        # add by anf
        for line in self.browse(cr, uid, ids).line_ids:
            line.write({'state': 'cancel'})
        # ## 
        return True

    def budget_done(self, cr, uid, ids, *args):
        for budget in self.browse(cr, uid, ids):
            budget.state = 'done'
            # add by anf
            for line in budget.line_ids:
                line.write({'state': 'done'})
            # ## 
        return True

    def compute_total(self, cr, uid, ids, name, arg, context=None):
        budget_line = self.browse(cr, uid, ids, context=None).line_ids
        for id in ids:
            total = 0
            res = {}
            for line in budget_line:
                total += line.planned_amount
            res[id] = total
        return res
    def compute_rem(self, cr, uid, ids, name, arg, context=None):
        res = dict.fromkeys(ids, 0)
        for id in ids:
            alloc = self.browse(cr, uid, id, context=None).amount_total
            resv = self.browse(cr, uid, id, context=None).res_count
            resv = resv.replace(',', '')  # menghilangkan , pada 100,000,120.00 agar menjadi 100000120.00
            rem = alloc - float(resv)
            res[id] = rem
        return res
    def is_readonly(self, cr, uid, ids, name, arg, context=None):
        # import ipdb;ipdb.set_trace();
        users_obj = self.pool.get('res.users')
        has_group = users_obj.browse(cr, uid, uid, context=None).has_group('brg_fin_sec.gr_budget_spv')
        res = {}
        for id in ids:
            state = self.browse(cr, uid, id, context=None).state
            if state == 'confirm':
                res[id] = True
            elif state == 'validate':
                res[id] = True
            elif state == 'done':
                res[id] = True
            elif state == 'suspend':
                if has_group == True:
                    res[id] = False
                else:
                    res[id] = True
            else:
                res[id] = False
        return res
    def is_readonly_fin_man(self, cr, uid, ids, name, arg, context=None):
        # import ipdb;ipdb.set_trace();
        users_obj = self.pool.get('res.users')
        has_group = users_obj.browse(cr, uid, uid, context=None).has_group('brg_fin_sec.gr_fin_man')
        res = {}
        for id in ids:
            state = self.browse(cr, uid, id, context=None).state
            if state == 'confirm':
                res[id] = True
            elif state == 'validate':
                res[id] = True
            elif state == 'done':
                res[id] = True
            elif state == 'suspend':
                if has_group == True:
                    res[id] = False
                else:
                    res[id] = True
            else:
                res[id] = False
        return res

    def _get_cc_id(self, cr, uid, context=None):
        users_obj = self.pool.get('res.users')
        cc_id = users_obj.browse(cr, uid, uid, context=None).cc_id.id
        # import ipdb;ipdb.set_trace();
        return cc_id
  
    def _user_views(self, cr, uid, ids, name, arg, context=None):
        """User mana saja yg bisa melihat budget ini"""
        res = dict.fromkeys(ids, 0)
        # Administrator
        for bdg_id in ids:
            res_ids = [1]
            # Yg terdapat di cost center nya
            users = self.browse(cr, uid, bdg_id).analytic_id.user_ids
            for user_id in users:
                res_ids.append(user_id.id)
                
            # budget spv
            group_spv = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'brg_fin_sec', 'gr_budget_spv')
            group_spv_id = group_spv and group_spv[1] or False
            spvs = self.pool.get('res.groups').browse(cr, uid, group_spv_id).users
            for spv in spvs:
                res_ids.append(spv.id)
            res[bdg_id] = res_ids
        return res
    
    def button_dummy(self, cr, uid, ids, context=None):
        return True  

    def unlink(self, cr, uid, ids, context=None):
        budget = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []
        # import ipdb;ipdb.set_trace();
        for s in budget:
            if s['state'] in ['draft', 'cancel']:
                unlink_ids.append(s['id'])
            else:
                raise osv.except_osv(('Invalid Action!'), ('In order to delete a Budget you must cancel it first.'))


        return super(brg_budget, self).unlink(cr, uid, unlink_ids, context=context)

    def res_count(self, cr, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, 0)
        # import ipdb;ipdb.set_trace();
        for budget in self.browse(cr, uid, ids, context=context):
            # jika status tidak validate atau suspend
            if budget.state not in ('validate', 'suspend'):
                res[budget.id] = "0.0"
                return res
            
            if budget.analytic_id:
                cr.execute("SELECT sum(amount) \
                            FROM brg_budget_log \
                            WHERE analytic_id in %s \
                            AND date >= %s\
                            AND date <= %s\
                            AND state = 'validate'\
                            ", (tuple([budget.analytic_id.id]), budget.date_from, budget.date_to,)
                            )  # harus menggunakan ',' pada belakang variable date_str
                hasil_query = cr.dictfetchall()
                sum = hasil_query[0]['sum']
                if sum is None:
                    sum = 0
                sum = '{0:,.2f}'.format(sum)
                # import ipdb;ipdb.set_trace();
                res[budget.id] = sum 
            else:
                res[budget.id] = "0.0"
        return res    
    
    @api.model
    def create(self, vals):
        budget_id = super(brg_budget, self).create(vals)
        for budget in budget_id:
            line_ids = budget.line_ids
            line_ids.write({'date_from': budget.date_from, 'date_to': budget.date_to, 'fiscalyear_id': budget.fiscalyear_id.id})
        return budget_id
    
    @api.multi
    def write(self, vals):
        for budget in self:
            line_ids = budget.line_ids
            line_ids.write({'date_from': budget.date_from, 'date_to': budget.date_to, 'fiscalyear_id': budget.fiscalyear_id.id})
        return super(brg_budget, self).write(vals)
    
    def action_src_budget_log_tree(self, cr, uid, ids, context=None):
        # import ipdb;ipdb.set_trace();
        act_obj = self.pool.get('ir.actions.act_window')
        mod_obj = self.pool.get('ir.model.data')
        cc_ids = []
        for budget in self.browse(cr, uid, ids, context=context):
            cc_ids += [x.id for x in budget.analytic_id]
        result = mod_obj.xmlid_to_res_id(cr, uid, 'brg_budget.action_src_budget_log_tree', raise_if_not_found=True)
        result = act_obj.read(cr, uid, [result], context=context)[0]
        result['domain'] = "[('analytic_id','in',[" + ','.join(map(str, cc_ids)) + "]),('date','<=','" + str(budget.date_to) + "')" + ",('date','>=','" + str(budget.date_from)
        result['domain'] = result['domain'] + "'),('state','=','validate')]"
        return result
    
    _name = "brg.budget"
    _description = "Budget AAI"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
        
    
    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('confirm', 'Waiting Validation'),
        ('validate', 'Validated'),
        ('suspend', 'Suspended'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')
    ]
    
    _columns = {
        'name': fields.char('Name', required=True,
                            ),  # Nanti diisi Budget Warehouse
        'is_template': fields.boolean('Template', copy=False),
        'notes': fields.text('Description', copy=False),
        'analytic_id': fields.many2one('account.analytic.account', 'Cost Center', copy=False, track_visibility='onchange',),  # nanti diisi cost Center
        'user_id': fields.many2one('res.users', 'Created by', copy=False, track_visibility='onchange',),
        'currency_id': fields.many2one('res.currency', 'Currency', copy=True),  # nanti diisi mata uang terkait
        'date_from': fields.date('Start Date', required=True,),
        'date_to': fields.date('End Date', required=True,),
        'state':fields.selection(STATE_SELECTION, "Status", track_visibility='onchange',),
        'line_ids':fields.one2many('brg.budget.line', 'budget_id', "Budget Line", copy=True,),
        'amount_max':fields.float('Maximum Amount Budget'),
        'res_count':fields.function(res_count, type="text", string='Reserved'),
        'is_readonly':fields.function(is_readonly, type="boolean", string='Is Readonly'),
        'is_readonly_fin_man':fields.function(is_readonly_fin_man, type="boolean", string='Is Readonly Finance Manager'),
        'amount_total': fields.function(compute_total, string='Amount Budget',
            store={
                'brg.budget': (lambda self, cr, uid, ids, c={}: ids, ['line_ids'], 10),
            },
            help="The total amount.", track_visibility='onchange',),
        'amount_rem': fields.function(compute_rem, string='Remaining Budget',
            help="The total Remaining amount"),
        'user_views': fields.function(_user_views, type="one2many", string='Users',),        
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year'),
    }
    _defaults = {
        'state':"draft",
        'user_id': lambda s, cr, uid, c: uid,
        'analytic_id': lambda self, cr, uid, ctx: self._get_cc_id(cr, uid, ctx),
        'currency_id': lambda self, cr, uid, context: self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.currency_id.id,
    }
    _order = "name"

class brg_budget_line(osv.Model):
    _name = "brg.budget.line"
    

    def _act_amt(self, cr, uid, ids, context=None):
        res = {}
        result = 0.0
        if context is None: 
            context = {}
        # import ipdb;ipdb.set_trace();
        for line in self.browse(cr, uid, ids, context=context):
            budget_obj = line.budget_id
            date_from = budget_obj.date_from
            date_to = budget_obj.date_to
            analytic_id = budget_obj.analytic_id.id
            account_id = line.account_id.id
            if account_id and analytic_id:
                cr.execute("SELECT SUM(amount) FROM account_analytic_line WHERE account_id=%s AND (date "
                           "between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) AND "
                           "general_account_id=%s", (analytic_id, date_from, date_to, account_id,))
                result = cr.fetchone()[0]
            if result is None:
                result = 0.00
            res[line.id] = -result
        return res

    def comp_act(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            # kolom Actual gak perlu ngitung kalau State dari Brg Budgetnya bukan Running atau pun Suspend
            if line.budget_id.state in ('validate', 'suspend', 'done'):
                res[line.id] = self._act_amt(cr, uid, [line.id], context=context)[line.id]
            else:
                res[line.id] = 0.0
        return res
    
    def _res_amt(self, cr, uid, ids, context=None):
        res = {}
        result = 0.0
        if context is None: 
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            budget_obj = line.budget_id
            date_from = budget_obj.date_from
            date_to = budget_obj.date_to
            analytic_id = budget_obj.analytic_id.id
            account_id = line.account_id.id
            if account_id and analytic_id:
                cr.execute("SELECT SUM(amount) FROM brg_budget_log \
                            WHERE analytic_id = %s \
                            AND (date between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) \
                            AND account_id = %s \
                            ", (analytic_id, date_from, date_to, account_id,))
                result = cr.fetchone()[0]
            if result is None:
                result = 0.00
            res[line.id] = result
        return res

    def comp_res(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            # kolom Reserved gak perlu ngitung kalau State dari Brg Budgetnya bukan Running atau pun Suspend
            if line.budget_id.state in ('validate', 'suspend', 'done'):
                res[line.id] = self._res_amt(cr, uid, [line.id], context=context)[line.id]
            else:
                res[line.id] = 0.0
        return res

    def comp_rem(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            # kolom Remaining gak perlu ngitung kalau State dari Brg Budgetnya bukan Running atau pun Suspend
            if line.budget_id.state in ('validate', 'suspend', 'done'):
                res[line.id] = float((line.planned_amount or 0.0) - line.res_amount)
            else:
                res[line.id] = 0.0
        return res
    
    def comp_act_rem(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            # kolom Remaining gak perlu ngitung kalau State dari Brg Budgetnya bukan Running atau pun Suspend
            if line.budget_id.state in ('validate', 'suspend', 'done'):
                res[line.id] = float((line.planned_amount or 0.0) - line.act_amount)
            else:
                res[line.id] = 0.0
        return res
    
    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('confirm', 'Waiting Validation'),
        ('validate', 'Validated'),
        ('suspend', 'Suspended'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')
    ]
    
    _columns = {
        'budget_id': fields.many2one('brg.budget', 'Related Budget', ondelete='cascade'),
        'account_id': fields.many2one('account.account', 'Account', domain=[('type', '!=', 'view')]),
        'analytic_id': fields.many2one('account.analytic.account', 'Cost Center'),  # nanti defaultnya mengikuti parent
        'state':fields.selection(STATE_SELECTION, "Status", track_visibility='onchange',),
        'planned_amount':fields.float('Planned Amount', required=True,),
        'res_amount': fields.function(comp_res, string='Reserved Amount',),
        'act_amount': fields.function(comp_act, string='Actual Amount',),
        'rem_amount': fields.function(comp_rem, string='Res. Remaining Amount',),
        'act_rem_amount': fields.function(comp_act_rem, string='Act. Remaining Amount',),
        'date_from': fields.date('Date From', required=True),
        'date_to': fields.date('Date To', required=True),
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year'),
    }
    _defaults = {
        'state': 'draft'
    }
    
    @api.model
    def check_total_budget_year(self, fiscalyear_id):
        bbl_line_ids = self.search([('fiscalyear_id', '=', fiscalyear_id)])
        total_budget_year = 0.0
        for bbl_line_id in bbl_line_ids:
            total_budget_year += bbl_line_id.planned_amount
        return total_budget_year or 0.0

class account_fiscalyear(models.Model):
    _inherit ="account.fiscalyear"
    
    total_budget = Fields.Float('Total Budget')