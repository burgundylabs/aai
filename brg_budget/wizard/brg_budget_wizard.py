
from openerp.osv import fields, osv
from openerp.tools.translate import _
import time

class brg_budget_done(osv.osv_memory):
    _name = 'brg.budget.done'
    
    def budget_done(self, cr, uid, ids, context):
        budget_ids = context.get('active_ids', [])
        for budget in self.pool.get('brg.budget').browse(cr, uid, budget_ids):
            if budget.state == 'validate':
                budget.state = 'done'
        return True    