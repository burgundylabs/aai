from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _

class report_account_budget(osv.osv_memory):
    _name = "report.account.budget"
    _description = "Budget Statistics"
    _order = 'period, analytic_account_id' 
    
    _columns = {
        'period': fields.char('Period', size=24, readonly=True, select=True),
        'budget_id': fields.many2one('brg.budget', 'Budget Name', readonly=True),
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic Account', select=True),
        'account_id': fields.many2one('account.account', 'Account', readonly=True, select=True),
        'planned_amount':fields.float('Planned Amount', readonly=True, digits_compute=dp.get_precision('Account')),
        'res_amount':fields.float('Reserved Amount', readonly=True, digits_compute=dp.get_precision('Account')),
        'act_amount':fields.float('Actual Amount', readonly=True, digits_compute=dp.get_precision('Account')),
        'rem_amount':fields.float('Remaining Amount', readonly=True, digits_compute=dp.get_precision('Account')),
        'act_rem_amount':fields.float('Act Remaining Amount', readonly=True, digits_compute=dp.get_precision('Account')),
        'state' : fields.selection([('draft', 'Draft'), 
                                    ('cancel', 'Cancelled'), 
                                    ('confirm', 'Confirmed'), 
                                    ('validate', 'Validated'),
                                    ('suspend', 'Suspend'),  
                                    ('done', 'Done')], 'Status', select=True, required=True, readonly=True),
    }
        
class wizard_account_budget(osv.osv_memory):
    _name = "wizard.account.budget"
    _description = "Wizard Budget Statistics"
    
    _columns = {
        'budget_ids': fields.many2many('brg.budget', string='Budget Name'),
        'account_id': fields.many2one('account.account', string='Account', domain=[('type', '!=', 'view')]),
        'mode': fields.selection([('all', 'Date'), 
                                  ('selected_budget', 'Selected Budget')], 'Filter by'),
        'date_from': fields.date('Start Date'),
        'date_to': fields.date('End Date'),
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year'),
        'state': fields.selection([('all', 'All (Except cancel)'), 
                                   ('not_draft', 'Validate Only')], 'Budget Line Status',
                                  help='All : All Budget lines status will displayed except Cancel (Draft, Confirm, Validate, Suspend, Done)')
    }
    
    _defaults = {  
        'state': 'all',  
        'mode': 'all'
    }
#     
    def view_budget(self, cr, uid, ids, context=None):
        wizard = self.browse(cr, uid, ids)
        line_obj = self.pool.get('brg.budget.line')
        cr.execute('TRUNCATE report_account_budget')
        dom = []
        if wizard.account_id:
            dom.append(('account_id', '=', wizard.account_id.id))
        if wizard.mode == 'all':
            dom.append(('date_to', '<=', wizard.date_to))
            dom.append(('date_from', '>=', wizard.date_from))
            if not wizard.date_from or not wizard.date_to:
                raise osv.except_osv(_('Warning!'), _("You must define Start date and End date!"))
        else:
            dom.append(('budget_id', 'in', wizard.budget_ids.ids))
            if not wizard.budget_ids:
                raise osv.except_osv(_('Warning!'), _("You must define Budget Name!"))   
               
        state = ['validate']
        if wizard.state == 'all':
            state.append('draft')
            state.append('confirm')
            state.append('suspend')
            state.append('done')
        dom.append(('state', 'in', state))
        
        line_ids = line_obj.search(cr, uid, dom)
        if not line_ids:
            raise osv.except_osv(_('Warning!'), _("No Budget Line Found!"))
        
        line_recs = line_obj.browse(cr, uid, line_ids)
        
        column = '(period, budget_id, account_id, analytic_account_id, \
                    planned_amount, res_amount, act_amount, rem_amount, act_rem_amount, state)'
        
        values = ''
        for line in line_recs:
            values += '('
            values += "'" + line.date_from + ' to ' + line.date_to + "', "
            values += '%s, %s, %s, %s, %s, %s, %s, %s, ' % (line.budget_id.id, \
                                                       line.account_id.id, \
                                                       line.budget_id.analytic_id.id, \
                                                       line.planned_amount, \
                                                       line.res_amount, \
                                                       line.act_amount, \
                                                       line.rem_amount, \
                                                       line.act_rem_amount)
            values += "'" + line.state + "'" + '), '
        cr.execute("INSERT INTO report_account_budget %s VALUES %s" % (column, values[:-2]))
        return {
            'name': _('Budget Report'),
            'view_mode': 'graph',
            'view_type': 'form',
            'context': {'graph_view_ref': 'brg_budget.view_account_budget_graph',
                        'search_view_ref': 'brg_budget.view_brg_budget_line_search',
                        },
            'res_model': 'report.account.budget',
            'type': 'ir.actions.act_window',
        }
        