from openerp.osv import orm, fields, osv
from openerp import api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta
from datetime import datetime
from lxml import etree

class res_company(osv.osv):
    _inherit = 'res.company'
    
    _columns = {
        'journal_ids': fields.many2many('account.journal', 'account_journal_company', 'company_id', 'journal_id', 'Allowed Journal',
                                        help="Allowed Journal to Manually Create", domain="[('type', '=', 'general')]"),
    }
    
class account_config_settings(osv.osv_memory):
    _inherit = 'account.config.settings'
     
    _columns = {
        'journal_ids': fields.related(
            'company_id', 'journal_ids',
            type='many2many',
            relation='account.journal',
            string="Allowed Journal",
            domain="[('type', '=', 'general')]"),
    }
    
    def onchange_company_id(self, cr, uid, ids, company_id, context=None):
        res = super(account_config_settings, self).onchange_company_id(cr, uid, ids, company_id, context=context)
        if company_id:
            company = self.pool.get('res.company').browse(cr, uid, company_id, context=context)
            res['value'].update({
                'journal_ids': (company.journal_ids or False),
                })
        else:
            res['value'].update({
                'journal_ids': False,
                })
        return res
    
class account_move_line(osv.osv):
    
    _inherit = "account.move.line"
    
    def write(self, cr, uid, ids, vals, context=None, check=False):
        if not isinstance(ids, tuple):
            line_ids = [ids]
        else:
            line_ids = ids
        for line_id in line_ids:
            an_acc_id = vals.get('analytic_account_id', 'not')
            if an_acc_id != 'not' and not an_acc_id:
                raise osv.except_osv(_('Warning !'), _('Please Input Analytic Account for this Transaction'))
        return super(account_move_line, self).write(cr, uid, ids, vals, context, check=False)
    
    def create(self, cr, uid, vals, context=None):
        acc_obj = self.pool.get('account.account')
        if acc_obj.browse(cr, uid, vals['account_id']).user_type.report_type in ('income', 'expense') \
            and not vals.get('analytic_account_id'):
            raise osv.except_osv(_('Warning !'), _('Please Input Analytic Account for this Transaction'))
        return super(account_move_line, self).create(cr, uid, vals, context)
    
    def _line_balance(self, cr, uid, ids, f_name, arg, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = line.debit - line.credit
        return res
    
    _columns = {
        'line_balance': fields.function(_line_balance, string='Balance'),
    }

class account_move(osv.osv):
    
    _inherit = "account.move"
    
    _columns = {
        'reversal_date' : fields.date('Reversal Date'),
        'reversal_state' : fields.selection([
            ('none', 'Not Aplicable'),
            ('reversed', 'Reversed'),
            ('2breversed', 'To Be Reversed'),
             ], 'Reversal Control', readonly=True),
        'manual' : fields.boolean('Manually Created'),
    }
    
    _defaults = {
        'reversal_state' : 'none',
        }
    
    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
         
        res = super(account_move, self).fields_view_get(cr, uid, view_id, view_type, context, toolbar, submenu=submenu)
        allowed_journal_ids = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.journal_ids.ids
        res['fields']['journal_id']['domain'] = "[('id', 'in'," + str(allowed_journal_ids) + " )]"
        return res
    
    def onchange_manual(self, cr, uid, ids, manual, context=None):
        if manual:
            allowed_journal_ids = self.pool.get('res.users').browse(cr, uid, uid).company_id.journal_ids.ids
            return {'domain': {'journal_id' : [('id', 'in', allowed_journal_ids)]}}
        else :
            return {'domain' : {'journal_id' : [('id', '!=', 0)]}}
    
    def do_reverse_journal(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        res = []
        new_line = {}
        for move in self.browse(cr, uid, ids) :
            if move.reversal_state != '2breversed' and not context.get('force_reverse'):
                continue
            dup = move.copy({'line_id':False})
            dup.date = fields.date.today()
            if move.reversal_state == '2breversed' :
                move.reversal_state = 'reversed'
                dup.reversal_state = 'none'
                dup.reversal_date = False
                dup.date = move.reversal_date
            dup.ref = move.ref + ' - Reversal'
            dup.period_id = self.pool.get('account.period').find(cr, uid, dup.date)[0]
            res.append(dup.id)
            for line in move.line_id:
                new_line[line.id] = line.copy({'name' : line.name + ' - Reversal',
                                      'move_id': dup.id,
                                      'debit' : line.credit,
                                      'credit' : line.debit,
                                      'amount_currency' :-line.amount_currency})
            for line in move.line_id:
                if line.account_id.reconcile :
                    self.pool.get('account.move.line').reconcile_partial(cr, uid, [new_line[line.id].id, line.id], type='auto', context=context)
        return res
    
    def reverse_journal(self, cr, uid, ids, context=None):
        context = context or {}
        context['force_reverse'] = True
        res = self.do_reverse_journal(cr, uid, ids, context)
        if not res:
            return False
        if len(res) > 1 :
            return {
                'domain': "[('id','in',[" + ','.join(map(str, list(res))) + "])]",
                'name': _('Reversed Journal Entries'),
                'view_mode': 'tree,form',
                'view_type': 'form',
                'context': {'tree_view_ref': 'account.view_move_tree', 'form_view_ref': 'account.view_move_form'},
                'res_model': 'account.move',
                'type': 'ir.actions.act_window',
            }
        else :
            return {
                'res_id': res[0],
                'name': _('Reversed Journal Entry'),
                'view_mode': 'form',
                'view_type': 'form',
                'context': {'form_view_ref': 'account.view_move_form'},
                'res_model': 'account.move',
                'type': 'ir.actions.act_window',
            }

    def auto_reverse_journal(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        context.update({
            'active_model': self._name,
            'active_ids': ids,
            'active_id': len(ids) and ids[0] or False,
            'wizard' : True,
        })
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.move.autoreverse',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context,
            'nodestroy': True,
            'auto_refresh':True,
        }
    
class account_bank_statement(osv.osv):
    _inherit = "account.bank.statement"
    
    def write(self, cr, uid, ids, vals, context=None):
        res = super(account_bank_statement, self).write(cr, uid, ids, vals, context)
        self._check_sign(cr, uid, ids, context)
        return res
    
    def create(self, cr, uid, vals, context=None):
        res = super(account_bank_statement, self).create(cr, uid, vals, context)
        self._check_sign(cr, uid, res, context)
        return res
    
    def _check_sign(self, cr, uid, ids, context=None):
        for statement in self.browse(cr, uid, ids):
            if statement.journal_id.type != 'bank':
                continue
            sign = False
            for line in statement.line_ids:
                if not sign :
                    sign = (line.amount > 0 and 1) or (line.amount < 0 and -1) or False
                elif sign != ((line.amount > 0 and 1) or (line.amount < 0 and -1)) :
                    raise osv.except_osv(_('Warning !'), _("Can't have different statement line sign (- and +) in one statement!"))
        return True
            
class account_period(osv.osv):
    _inherit = "account.period"
    
    
    _columns = {
        'state': fields.selection([('draft', 'Open'), ('done', 'Closed')], 'Status', readonly=True, copy=False,
                                  help='When monthly periods are created. The status is \'Draft\'. At the end of monthly period it is in \'Done\' status.'),
    }
    
    _defaults = {
        'state': 'done',
    }
    
    def action_draft(self, cr, uid, ids, context=None):
        # TODO: Uncomment when Go Live
        open_period_ids = self.search(cr, uid, [('state', '=', 'draft')])
        if open_period_ids or (isinstance(ids, tuple) and len(ids) > 1):
            raise osv.except_osv(_('Warning!'), _('You can not have more than one open period'))
        super(account_period, self).action_draft(cr, uid, ids, context) 
        
class account_statement_from_invoice_lines(osv.osv_memory):
    
    _inherit = "account.statement.from.invoice.lines"
    
    def populate_statement(self, cr, uid, ids, context=None):
        context = dict(context or {})
        statement_id = context.get('statement_id', False)
        if not statement_id:
            return {'type': 'ir.actions.act_window_close'}
        data = self.read(cr, uid, ids, context=context)[0]
        line_ids = data['line_ids']
        if not line_ids:
            return {'type': 'ir.actions.act_window_close'}

        line_obj = self.pool.get('account.move.line')
        statement_obj = self.pool.get('account.bank.statement')
        statement_line_obj = self.pool.get('account.bank.statement.line')
        currency_obj = self.pool.get('res.currency')
        statement = statement_obj.browse(cr, uid, statement_id, context=context)
        line_date = statement.date
        
        reconcile_partial_ids = []

        # for each selected move lines
        for line in line_obj.browse(cr, uid, line_ids, context=context):
            ctx = context.copy()
            #  take the date for computation of currency => use payment date
            ctx['date'] = line_date
            amount = 0.0

            if line.debit > 0:
                amount = line.debit
            elif line.credit > 0:
                amount = -line.credit

            if line.amount_currency:
                if line.company_id.currency_id.id != statement.currency.id:
                    # In the specific case where the company currency and the statement currency are the same
                    # the debit/credit field already contains the amount in the right currency.
                    # We therefore avoid to re-convert the amount in the currency, to prevent Gain/loss exchanges
                    amount = currency_obj.compute(cr, uid, line.currency_id.id,
                        statement.currency.id, line.amount_currency, context=ctx)
            elif (line.invoice and line.invoice.currency_id.id != statement.currency.id):
                amount = currency_obj.compute(cr, uid, line.invoice.currency_id.id,
                    statement.currency.id, amount, context=ctx)
            elif line.reconcile_partial_id:
                amount = reduce(lambda y, t: (t.debit or 0.0) - (t.credit or 0.0) + y, line.reconcile_partial_id.line_partial_ids, 0.0)

            context.update({'move_line_ids': [line.id],
                            'invoice_id': line.invoice.id})
            
            if not line.reconcile_partial_id or line.reconcile_partial_id.id not in reconcile_partial_ids:
                statement_line_obj.create(cr, uid, {
                    'name': line.name or '?',
                    'amount': amount,
                    'partner_id': line.partner_id.id,
                    'statement_id': statement_id,
                    'ref': line.ref,
                    'date': statement.date,
                    'amount_currency': line.amount_currency,
                    'currency_id': line.currency_id.id,
                }, context=context)
                reconcile_partial_ids.append(line.reconcile_partial_id.id)
        return {'type': 'ir.actions.act_window_close'}
