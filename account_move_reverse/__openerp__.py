{
    "name": "Reverse Journal Entry",
    "version": "1.0",
    "depends": ['account'],
    "author":"PT. VISI",
    "category":"Accounting",
    "description" : """Automatically or Manually Reverse Journal Entry""",
    'data': [
        'account_view.xml',
        'wizard/account_move_autoreverse_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
