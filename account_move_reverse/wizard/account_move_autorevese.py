
from openerp.osv import fields, osv
from openerp.tools.translate import _
import time

class account_move_autoreverse(osv.osv_memory):
    
    _name = "account.move.autoreverse"
    _description = "Autoreverse Creation"
    
    _columns = {
        'reversal_date' : fields.date('Reversal Date'),
    }
    
    def do_autoreverse(self, cr, uid, ids, context=None):
        reversal_date = self.browse(cr, uid, ids).reversal_date
        for move in self.pool.get('account.move').browse(cr, uid, context.get('active_ids', [])) :
            if move.reversal_state not in ['2breversed','reversed'] :
                move.write({'reversal_date' : reversal_date, 'reversal_state' : '2breversed'})
            else :
                raise osv.except_osv(_('Error!'), _('Unable to reverse this journal'))
        return {
                'type': 'ir.actions.act_window_close',
            }

class account_move_autoreverse_generate(osv.osv_memory):

    _name = "account.move.autoreverse.generate"
    _description = "Autoreverse Compute"
    _columns = {
       'date': fields.date('Generate Entries Before', required=True),
    }
    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d'),
    }

    def compute_autoreverse(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('account.move')
        date = self.read(cr, uid, ids, context=context)[0]['date']
        ids = move_obj.search(cr, uid, [('reversal_state', '=', '2breversed'), ('reversal_date', '<', date)])
        if len(ids) == 0 :
            return {
                'type': 'ir.actions.act_window_close',
            }
        else :
            return move_obj.reverse_journal(cr, uid, ids, context)
    
    #tambahan anif
    def run_scheduler(self, cr, uid, context=None):
        move_obj = self.pool.get('account.move')
        date = time.strftime('%Y-%m-%d')
        ids = move_obj.search(cr, uid, [('reversal_state', '=', '2breversed'), ('reversal_date', '<=', date)])
        move_obj.reverse_journal(cr, uid, ids, context)
        return True
        
class account_move_post(osv.osv_memory):
    _name = 'account.move.post'
    
    def button_post(self, cr, uid, ids, context=None):
        for move in self.pool.get('account.move').browse(cr, uid, context.get('active_ids', [])):
            if move.state == 'draft':
                move.button_validate()
        return {
            'type': 'ir.actions.act_window_close',
        }
    
    def reverse_journal(self, cr, uid, ids, context=None):
        return self.pool.get('account.move').browse(cr, uid, context.get('active_ids', [])).reverse_journal()
            